CONTENTS OF THIS FILE
---------------------
* Introduction
* Audience
* Supported field types and operations
* Requirements
* Installation
* Configuration
* Usage
* Troubleshooting
* Maintainers


INTRODUCTION
------------
This module is meant for individual field data manipulation from commandline to
add, delete or modify values in fields. It uses Drush commandline interface.
See http://drush.org/ for Drush. New functionality is added by feature
requests. Aim is to be able add, delete, modify and get data in all core field
types and most contributed field types. This project is currently located at:

* For a full description of the module, visit the project page:
  https://drupal.org/sandbox/ragnarkurm/2274189
* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2274189


AUDIENCE
--------
This module is mostly for developers, admins, but not limited to.

* Admins
  * Quick data inspection
  * Quick data fixes
  * Helpful in data analyses
  * Helpful for finishing up migration
* Developers
  * Integration with other systems
  * External data processing
  * Asynchronous data manipulation
  * Testing for module development
* Regular user
  * Importing directoryful of files or images


SUPPORTED FIELD TYPES AND OPERATIONS
------------------------------------
* Count, Delete
  * All field types
* List, Modify (Add, Set)
  * Date
  * Datestamp
  * Datetime
  * Email
  * Entity Reference
  * File
  * Image
  * Link
  * List: Boolean
  * List: Float
  * List: Integer
  * List: Text
  * Number: Decimal
  * Number: Float
  * Number: Integer
  * Taxonomy Term Reference
  * Text
  * Text Long
  * Text with Summary
* Node Title
  * Find
  * Replace
  * Get
  * Set


REQUIREMENTS
------------
* Drush is installed
  https://github.com/drush-ops/drush
* Field module (in core) is enabled
* Entity module in installed and enabled
  https://drupal.org/project/entity
* Relevant field module to be enabled. (Example: If you want to
  add/del/get data in a file field, then you need File module enabled.)
* This module assumes there are some fields with that type to work with.
* You have commandline (SSH) access to your Drupal installation.
* Understanding about following concepts:
  * Entity type (Node, User, Taxonomy Term, Comment, Profile2, ...)
  * Bundle (Article, Basic Page, ...)
  * Entity ID (1, 100, 1234, ...)
  * Field name (field_pdf, field_thumb, ...)
  * Machine name (only_lowercase_letters_and_underscores)
  * Field type (File, Image, Email, Text, Integer, ...)
  * Drush
  * Unix shell

An Introduction to Entities: https://drupal.org/node/1261744
About Drush: http://www.drush.org/about
GNU Bash: http://www.gnu.org/software/bash/


INSTALLATION
------------
1) Download the module
   cd ~/.drush
   git clone --branch 7.x-1.x \
     http://git.drupal.org/sandbox/ragnarkurm/2274189.git drush_fields
2) Refresh Drush commands cache
   drush cc drush


CONFIGURATION
-------------
This module has nothing to configure.


USAGE
-----

Generally command line consists of 5 parts
. . . . . . . . . . . . . . . . . . . . . 

1) drush
2) command - For example: fields-mod, fields-node-title-get, etc
3) target arguments - This specific to each command
   and it specifies which data to access.
   Please check drush command --help.
   For example: node 1234 field_myfile 6
4) required options - For example: --value=/tmp/a.txt
5) other optional - For example: --mime=text/plain

Commandline examples
. . . . . . . . . . 

* Set a file field to a certain file:
  drush fields-mod --set node 1 field_myfilefield \
    --value=/tmp/icon-flag.pdf \
    --transfer=copy:rename

* Add a file to a file field:
  drush fields-mod --add node 1 field_myfilefield \
    --value=/tmp/icon-flag.pdf \
    --transfer=copy:rename

* Add a directoryful of files to a file field:
  drush fields-mod --add node 1 field_myfilefield \
    --value=/home/username/docs \
    --transfer=copy:rename

* Delete an element from file field:
  drush fields-del node 1 field_myfilefield 1

* List elements in a field:
  drush fields-get node 1 field_file
Delta FID UID MIME Type       TimeStamp            Size Filename
    0  30   1 application/pdf 2014.Jun.06 11:39  184736 icon-flag.pdf
    1  31   1 text/plain      2014.Jun.06 14:20     201 upt_1466756264.txt
    2  32   1 text/plain      2014.Jun.06 14:20     201 upt_1466756264.txt
    3  33   1 text/plain      2014.Jun.06 14:21 2097152 a.txt

* List elements in a field with custom template:
  drush fields-get node 1234 field_pdf --template='{fid}:{filename}'
  42:favicon.ico
  43:a.txt

* Set node title:
  drush fields-node-title-set 1 --value='Just testing'

* Get node title:
  drush fields-node-title-get 1
  Just testing

Detailed help
. . . . . . .

drush help
drush fields-count --help
drush fields-del --help
drush fields-get --help
drush fields-mod --help
drush fields-node-title-find --help
drush fields-node-title-get --help
drush fields-node-title-replace --help
drush fields-node-title-set --help


SECURITY AND DATA ACCESS
------------------------
Drush default Anonymous user and user 1 can do all operations
for practical reasons.
All other users (-u or --user) are checked if they can access particular data.
This is done on two levels: entity level and field level.
This data access approach may change according to feedback.


TROUBLESHOOTING
---------------
* This module contains a lot of checks and verbose error messages which
  should be helpful for solving problems.
* Check: drush command --help
* Check watchdog log
* Post an issue:
  https://drupal.org/project/issues/2274189


RELATED PROJECTS
----------------
* Drush Entity
https://www.drupal.org/project/drush_entity
* Drush Extras
https://www.drupal.org/project/drush_extras
* File Entity
https://www.drupal.org/project/file_entity
* Field Permissions
https://www.drupal.org/project/field_permissions


MAINTAINERS
-----------
Current maintainers:
* Ragnar Kurm (ragnarkurm) - https://drupal.org/u/ragnarkurm
