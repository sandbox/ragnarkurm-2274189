<?php

/**
 * @file
 * Provide Drush commands and implement common functions.
 *
 * This file 1) builds and provides Drush commands hook 2) and implements common
 * functions across different fields.
 */

require_once 'fields/_commands.inc';
foreach (drush_fields_commands() as $c) {
  require_once "fields/$c.inc";
}

require_once 'fields/DrushFields.inc';

/**
 * Implements hook_drush_command().
 */
function drush_fields_drush_command() {

  $items = array();
  foreach (drush_fields_commands() as $c) {
    $f = "drush_fields_{$c}_cmd";
    $items = array_merge($items, $f());
  }

  return $items;

}

/**
 * Provide description for --save option for hook_drush_command().
 *
 * @return string
 *   Return description text.
 */
function drush_fields_save_options_description() {
  $text = array(
    'Optional. How to save an entity? Default = entity_save.',
    'entity_save - High-level save whole entity and run Rules and all hooks (clear cache, queue for search indexing etc).',
    'field_attach_update - Middle-level save, dont run high-level hooks. Clear entity\'s cache.',
  );
  $text = implode("\n", $text);
  return $text;
}

/**
 * Process --save option.
 *
 * @return string|bool
 *   Return save type as string or FALSE on error.
 */
function drush_fields_arg_save() {

  $ok = array(
    'entity_save' => TRUE,
    'field_attach_update' => TRUE,
    // 'field_attach_update-cache' => TRUE,
    // 'field_sql_storage_field_storage_write+cache' => TRUE,
    // 'field_sql_storage_field_storage_write-cache' => TRUE,
  );

  $save = drush_get_option('save', 'entity_save');
  if ($ok[$save]) {
    return $save;
  }

  drush_set_error(dt('--save=!save: Invalid option.', array('!save' => $save)));
  return drush_fields_options(array_keys($ok));

}

/**
 * Process general y/n.
 *
 * Turn different representations to boolean.
 * If $values is NULL, then return $default.
 *
 * @param string $value
 *   Value to convert.
 * @param string $default
 *   Default value.
 *
 * @return mixed
 *   Boolean - if $value was converted.
 *   $default - if $values was NULL.
 *   NULL - if error occurred.
 */
function drush_fields_yn($value, $default = NULL) {

  $type = gettype($value);
  switch ($type) {

    case 'NULL':
      return $default;

    case 'boolean':
      return $value;

    case 'string':
      $value = strtolower($value);
      if (preg_match('/^(yes|y|true|on|1)$/', $value)) {
        return TRUE;
      }
      if (preg_match('/^(no|n|false|off|0)$/', $value)) {
        return FALSE;
      }
      drush_set_error(dt('\'!value\': Expected: yes/no, y/n, true/false, on/off, 1/0.', array('!value' => $value)));
      return NULL;

    default:
      drush_set_error(dt('Internal error. Invalid option type. Type = !type', array('!type' => $type)));
      return NULL;

  }

}

/**
 * Return description for --help.
 *
 * Note that drush command descriptions are untranslated.
 *
 * @param string $default
 *   If specified, it will be shown as default value.
 *
 * @return string
 *   Return description string.
 */
function drush_fields_yn_desc($default = NULL) {
  $default = empty($default) ? '' : " Default = $default";
  return 'Possible values: yes/no, y/n, true/false, on/off, 1/0.' . $default;
}

/**
 * Process "--value". Read data from different sources.
 *
 * If starts with slash '/' then assume absolute file path and read the file.
 * If starts with slash 'proto://' then assume URL and fetch the file.
 * If equals dash '-' then assume URL and read stdin.
 * Or assume string.
 *
 * @param string $value
 *   Commandline --value value.
 *
 * @return string|bool
 *   Return string when successful.
 *   Return FALSE on error.
 */
function drush_fields_arg_data($value) {

  if (preg_match('!^/!', $value)) {
    $value = file_get_contents($value);
    if ($value === FALSE) {
      return drush_set_error(dt('--value: Unable to read file.'));
    }
    return $value;
  }

  if (preg_match('!^[a-zA-Z]+://!', $value)) {
    $value = file_get_contents($value);
    if ($value === FALSE) {
      return drush_set_error(dt('--value: Unable to fetch file.'));
    }
    return $value;
  }

  if ($value === '-') {
    $value = file_get_contents('php://stdin');
    if ($value === FALSE) {
      return drush_set_error(dt('--value: Unable to read stdin.'));
    }
    return $value;
  }

  return $value;

}

/**
 * Return description for --help.
 *
 * Note that drush command descriptions are untranslated.
 *
 * @return string
 *   Return description string.
 */
function drush_fields_arg_data_desc() {
  return 'If begins with \'/\' assume file, if begin with \'proto://\' assume URL, if is \'-\' assume stdin, or assume string.';
}

/**
 * Helper function to prevent duplicate elements.
 *
 * Checks for existing data if --op=add.
 * This function works in two modes: init and check.
 * In init mode array of existing keys are passed in.
 * In check mode only chekup key is passed in.
 * If fails then error is issued.
 *
 * @param array|string $data
 *   Array - perform init with array values as keys.
 *   String - perform checkup with key.
 *
 * @return bool|NULL
 *   Return FALSE if duplication is detected.
 *   Return NULL if ok.
 */
function drush_fields_dups($data) {

  static $cache;

  /* Perform init. */

  if (is_array($data)) {
    $cache = array();
    if (drush_get_option('add', NULL) === TRUE) {
      foreach ($data as $key) {
        $cache[$key] = TRUE;
      }
    }
    /* Return NULL for PAReview. */
    return NULL;
  }

  /* Check and record */

  if ($cache[$data]) {
    return drush_set_error(dt('!key: Skipping duplicate element.', array('!key' => $data)));
  }

  $cache[$data] = TRUE;

}

/**
 * Handle --timezone.
 *
 * @return object|bool
 *   Return DateTimeZone object.
 *   Return FALSE on failure.
 */
function drush_fields_arg_timezone() {

  $tz = drush_get_option('timezone', NULL);
  if (empty($tz) && $tz !== '0') {
    return drush_set_error(dt('--timezone: Required.'));
  }

  $value = timezone_open($tz);
  if ($value === FALSE) {
    return drush_set_error(dt('!tz: Invalid timezone.', array('!tz' => $tz)));
  }

  return $value;
}

/**
 * Convert common timestamps to date object.
 *
 * @param string $datetime
 *   One of following formats:
 *   - Seconds since epoch.
 *   - YYYY-MM-DDTHH:MM:SS (ISO format).
 *   - YYYY-MM-DD HH:MM:SS.
 *   - strtotime() format.
 * @param object|string $tz
 *   Timezone string or DateTimeZone object.
 *
 * @return object|bool
 *   Replace DateTime object.
 *   Return FALSE on failure.
 */
function drush_fields_date($datetime, $tz) {

  if (is_string($tz)) {
    $tz = timezone_open($tz);
    if ($tz === FALSE) {
      return drush_set_error(dt('!tz: Invalid timezone.', array('!tz' => $tz)));
    }
  }

  if (preg_match('/^\d+$/', $datetime)) {
    // Cannot set timezone directly, so work around back-and-fourth.
    $d = date('Y-m-d H:i:s', $datetime);
    // Too big number fail here.
    $d = date_create_from_format('Y-m-d H:i:s', $d, $tz);
    return $d;
  }

  if (preg_match('/^[\d-]+T[\d:]+$/', $datetime)) {
    $datetime = preg_replace('/T/', ' ', $datetime);
    $d = date_create_from_format('Y-m-d H:i:s', $datetime, $tz);
    if (empty($d)) {
      return drush_set_error(dt('!d: Invalid date/time format.', array('!d' => $datetime)));
    }
    return $d;
  }

  if (preg_match('/^[\d-]+ [\d:]+$/', $datetime)) {
    $d = date_create_from_format('Y-m-d H:i:s', $datetime, $tz);
    if (empty($d)) {
      return drush_set_error(dt('!d: Invalid date/time format.', array('!d' => $datetime)));
    }
    return $d;
  }

  $epoch = strtotime($datetime);
  if ($epoch === FALSE) {
    return drush_set_error(dt('!d: Invalid date/time format.', array('!d' => $datetime)));
  }
  // Cannot set timezone directly, so work around back-and-fourth.
  $d = date('Y-m-d H:i:s', $epoch);
  $d = date_create_from_format('Y-m-d H:i:s', $d, $tz);
  return $d;

}

/**
 * Print list of options to user.
 *
 * @param array $list
 *   List of options as array values.
 *
 * @return bool
 *   FALSE.
 */
function drush_fields_options(array $list, $header = NULL) {
  if (empty($header)) {
    $header = dt('Possible options:');
  }
  drush_set_error($header);
  foreach ($list as $v) {
    drush_set_error("* $v");
  }
  return FALSE;
}
