<?php

/**
 * @file
 * Command implementations for Entity Reference field.
 */

class DrushFieldsFieldEntityreference extends DrushFields {

  /**
   * Field-specific command description implementation.
   *
   * @return array
   *   Field-specific command description.
   */
  static public function updateCmd() {

    return array(
      'examples' => array(
        'drush fields-update node 1234 field_pages --op=set --value=\'1,2,3\''
        => 'Set field_pages to refer to nodes with id 1, 2 and 3 of node 1234.',
        'drush fields-update node 1234 field_people --op=set --value=\'1,2,3\''
        => 'Set field_people to refer to users with id 1, 2 and 3 of node 1234.',
      ),
    );

  }

  /**
   * Field-specific implementation of update preparation.
   *
   * @param string $values
   *   Command-line argument --value value.
   *
   * @return array
   *   Field-specific values to set/add.
   */
  protected function updatePrepare($values) {

    $bundles = array();
    foreach ($this->general['settings']['handler_settings']['target_bundles'] as $b) {
      $bundles[$b] = TRUE;
    }
    $type = $this->general['settings']['target_type'];

    // Init duplication check.
    $data = array();
    foreach ($this->values as $t) {
      $t = entity_metadata_wrapper($type, $t);
      $data[] = (int) $t->getIdentifier();
    }
    drush_fields_dups($data);

    $ret = array();

    $values = explode(',', $values);
    $values = preg_replace('/\s+/', '', $values);
    foreach ($values as $id) {
      if (!preg_match('/^\d+$/', $id)) {
        return $this->errorSet(dt('!id: Invalid Entity ID, integer expected.', array('!id' => $id)));
      }
      $id = (int) $id;
      $entity = entity_load_single($type, $id);
      if (empty($entity)) {
        return $this->errorSet(dt('!id: No such entity of type !type.', array('!id' => $id, '!type' => $type)));
      }
      if (!empty($bundles)) {
        $entity = entity_metadata_wrapper($type, $entity);
        $bundle = $entity->getBundle();
        if (!$bundles[$bundle]) {
          return $this->errorSet(dt('!id, !bundle: Entity belongs to non-allowed bundle.', array('!id' => $id, '!bundle' => $bundle)));
        }
      }

      if (drush_fields_dups($id) === FALSE) {
        continue;
      }

      $ret[] = $entity->value();
    }

    return $ret;

  }

  /**
   * Field-specific 'read' template.
   *
   * @return string
   *   Return template string.
   */
  public function defaultTemplate() {
    return '{%6d:delta} {%6d:id} {%-9s:type} {%-9s:bundle} {%s:label}';
  }

  /**
   * Field-specific 'read' keys.
   *
   * @return array
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {

    return array(
      'delta' => new DrushFieldsReadFormatterDelta(),
      'type' => new DrushFieldsReadFormatterEntityType('type', dt('Type'), dt('Entity Type.')),
      'bundle' => new DrushFieldsReadFormatterEntityType('bundle', dt('Bundle'), dt('Entity Bundle.')),
      'id' => new DrushFieldsReadFormatterEntityType('id', dt('ID'), dt('Entity ID.')),
      'label' => new DrushFieldsReadFormatterEntityType('label', dt('Label'), dt('Entity Label (Title).')),
    );

  }

}
