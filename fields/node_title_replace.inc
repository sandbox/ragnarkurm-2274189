<?php

/**
 * @file
 * Node title replace functionality.
 */

/**
 * Provide Drush commands this "plugin" can handle.
 *
 * @return array
 *   Return an array of Drush commands.
 */
function drush_fields_node_title_replace_cmd() {

  return array(

    'fields-node-title-replace' => array(
      'description' => 'Replace node titles by regular expression.',
      'callback' => 'drush_fields_node_title_replace',
      'examples' => array(
        'drush fields-node-title-replace --rx=\'/(\d+)/\' --value=\'#$1\''
        => 'Prefix numbers by #.',
      ),
      'options' => array(
        'rx' => 'Required. Perl Compatible Regular Expression (PHP PCRE). See preg_replace(). Must specify delimiters, may have modifiers. Example: \'/abc(\w)/i\'.',
        'value' => 'Required. Replacement value. Supports backreferences as $n. See preg_replace(). Example: \'xyz$1\'.',
        'logging' => 'Optional. Log all changes to watchdog or not? ' . drush_fields_yn_desc('y'),
      ),
      /* Have to be able to use --user option. */
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
      'core' => array('7'),
      'drupal dependencies' => array(
        'entity',
        'node',
      ),
    ),

  );

}

/**
 * Perform actual title manipulation.
 *
 * Print stdout Node ID and Node title. Uses PHP PCRE.
 */
function drush_fields_node_title_replace() {

  $rx = drush_get_option('rx', NULL);
  if (empty($rx) && $rx !== '0') {
    return drush_set_error(dt('--rx: Required.'));
  }

  $value = drush_get_option('value', NULL);
  if (empty($value) && $value !== '0') {
    return drush_set_error(dt('--value: Required.'));
  }

  $logging = drush_fields_yn(drush_get_option('logging', TRUE), TRUE);
  if (is_null($logging)) {
    return FALSE;
  }

  // Do not suppress by @.
  if (is_null(preg_replace($rx, $value, ''))) {
    return drush_set_error(dt('Invalid regular expression or replacement value.'));
  }

  if (!drush_confirm(dt('Replace node titles?'))) {
    drush_user_abort();
  }

  $result = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->orderby('nid')
    ->execute();

  $count = 0;
  while ($n = $result->fetchAssoc()) {
    $nid = $n['nid'];
    $title = $n['title'];
    $node = node_load($nid);
    if (!drush_fields_access('view', 'node', $node)) {
      continue;
    }
    $new = preg_replace($rx, $value, $title);
    if ($new === $title) {
      continue;
    }
    $node->title = $new;
    node_save($node);
    $count = $count + 1;
    drush_print(dt('Old: !nid: !title', array('!nid' => $nid, '!title' => $title)));
    drush_print(dt('New: !nid: !title', array('!nid' => $nid, '!title' => $new)));

    if ($logging) {
      $link = NULL;
      $uri = entity_uri('node', $node);
      if (!empty($uri['path'])) {
        $link = l(dt('View'), $uri['path']);
      }
      watchdog(
        'drush_fields',
        'Operation = !op. Entity type = !type. Bundle = !bundle. ID = !id. Field = !field.',
        array(
          '!op' => dt('Update'),
          '!type' => 'node',
          '!bundle' => $node->type,
          '!id' => $nid,
          '!field' => 'title',
        ),
        WATCHDOG_INFO,
        $link
      );
    }

  }

  drush_print(dt('Total !count replacements.', array('!count' => $count)));

}
