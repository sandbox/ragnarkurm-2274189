<?php

/*
TODO:
- access security
- condition and format variables are different: entity_id vs id, entity_type vs type, etc
- get rid of errorSetOptions ?
- universal field, entity, bundle checking funcs.
- ReadFormatter -> Formatter
- Formatter - make it non-field-specific
*/

/**
 * @file
 * A Field data find functionality.
 */

/**
 * Provide Drush commands this "plugin" can handle.
 *
 * @return array
 *   Return an array of Drush commands.
 */
function drush_fields_find_cmd() {

  return array(

    'fields-find' => array(
      'description' => 'Find data elements across all entities in given field.',
      'callback' => 'drush_fields_find',
      'arguments' => array(
        'field machine name' => 'Required. Example: field_message',
      ),
      'options' => array(
        'template' => 'Optional. Custom output template.'
        . "\nLeave empty to get list of options.",
        'header' => 'Optional. Display header or not? ' . drush_fields_yn_desc('y'),
      ),
      'examples' => array(
        'drush fields-find field_text \'entity_type = node\' \'value ~ /this|that/\''
        => 'Find nodes where field_text contains \'this\' or \'that\'.',
        'drush fields-find field_dateiso \'bundle = page\' \'entity_id in 1,2,3\' \'value2 !~ /^1999/\''
        => 'Find entities having bundle \'page\' and having id 1, 2 or 3 where field_dateiso value2 does not begin with \'1999\'.',
      ),
      'required-arguments' => 1,
      /* Have to be able to use --user option. */
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
      'core' => array('7'),
      'drupal dependencies' => array(
        'entity',
        /* The field module */
      ),
    ),

  );

}

/**
 * Implements find.
 *
 * @param string $field_name
 *   Field machine name. Example: field_count.
 *
 * @return bool|NULL
 *   NULL - success.
 *   FALSE - failure.
 */
function drush_fields_find($field_name = NULL) {

  $args = drush_get_arguments();
  // Drop command.
  array_shift($args);
  $c = count($args);

  if ($c < 1) {
    return drush_set_error(dt('Expected 1-3 arguments.'));
  }
  // Field name.
  if ($c > 0) {
    $field_name = array_shift($args);
    $c--;
    $general = field_info_field($field_name);
    if (empty($general)) {
      return drush_set_error(dt('!f: No such field.', array('!f' => $field_name)));
    }
  }

  $var_types = array(
    'bundle' => 'string',
    'delta' => 'integer',
    'entity_id' => 'integer',
    'entity_type' => 'string',
    'language' => 'string',
    'revision_id' => 'integer',
  );

  $var_map = array(

    'char' => 'string',
    'varchar' => 'string',
    'text' => 'string',
    'tinytext' => 'string',
    'mediumtext' => 'string',
    'longtext' => 'string',

    'integer' => 'integer',
    'int' => 'integer',
    'smallint' => 'integer',
    'tinyint' => 'integer',
    'mediumint' => 'integer',
    'bigint' => 'integer',

    'decimal' => 'float',
    'numeric' => 'float',

    'float' => 'float',
    'double' => 'float',

  );

  /* Extract all needed info from field general info. */

  $current = &$general['storage']['details']['sql']['FIELD_LOAD_CURRENT'];
  if (count($current) != 1) {
    return drush_set_error(dt('Unable to pin-point database table to work with.'));
  }
  $tbl = array_keys($current);
  $tbl = array_shift($tbl);

  $db_fields = &$current[$tbl];
  $drupal_fields = &$general['columns'];

  /* </extract> */

  // Build list of accepted variables.
  foreach ($drupal_fields as $k => $v) {
    $type = $v['type'];
    if (empty($var_map[$type])) {
      continue;
    }
    $var_types[$k] = $var_map[$type];
  }

  $db_conditions = array();
  $php_conditions = array();
  while ($c) {
    $condition = array_shift($args);
    $c--;
    $tmp = explode(' ', $condition, 3);
    if (count($tmp) != 3) {
      return drush_set_error(dt('\'!cond\': Invalid condition. Expected format \'variable op value\' which is 3 parts separated by spaces. Use single quotes.', array('!cond' => $condition)));
    }
    list($var, $op, $value) = $tmp;

    $rx = '/^[a-z0-9_]+$/';
    if (!preg_match($rx, $var)) {
      return drush_set_error(dt('\'!var\': Invalid variable. Must match regex !rx.', array('!var' => $var, '!rx' => $rx)));
    }

    /*
     * We have two level of filtering:
     * fast database 'where query'
     * and slow php processing.
     */

    $cond = $var_types[$var];
    if (empty($cond)) {
      drush_set_error(dt('\'!var\': Unknown variable.', array('!var' => $var)));
      $options = array();
      foreach ($var_types as $k => $v) {
        $options[] = "$k ($v)";
      }
      return drush_fields_options($options);
    }
    $cond = "drush_fields_find_$cond";
    $cond = $cond($op, $value);
    if ($cond === FALSE) {
      return FALSE;
    }
    list($db_cond, $php_cond) = $cond;

    // Translate only field-specific variable to database column name.
    if (!empty($db_fields[$var])) {
      $var = $db_fields[$var];
    }

    if (!empty($db_cond)) {
      $db_cond = array_merge(array($var), $db_cond);
      $db_conditions[] = $db_cond;
    }

    if (!empty($php_cond)) {
      $php_cond = array_merge(array($var), $php_cond);
      $php_conditions[] = $php_cond;
    }
  }

  /*
  * Preliminary argument processing is done
  * now build the all-mighty query.
  */

  $q = db_select($tbl, 'f');

  // Fetch all fields.
  // Translate only field-specific variables to database column names.
  $fields = array_keys($var_types);
  foreach ($fields as &$f) {
    if (!empty($db_fields[$f])) {
      $f = $db_fields[$f];
    }
  }
  $q->fields('f', $fields);

  // Build conditions.
  $q->condition('deleted', 0);
  foreach ($db_conditions as $c) {
    list($var, $value, $op) = $c;
    $q->condition($var, $value, $op);
  }

  $q->orderBy('entity_id', 'ASC');
  $q->orderBy('delta', 'ASC');

  require_once 'DrushFieldsFind.inc';
  $find = new DrushFieldsFind();
  foreach ($db_fields as $drupal_field => $db_field) {
    $type = $var_types[$drupal_field];
    if (empty($type)) {
      continue;
    }
    $tmp = $find->addSpecial($drupal_field, $db_field, $var_types[$drupal_field]);
    if ($tmp === FALSE) {
      return FALSE;
    }
  }
  $template = drush_fields_arg_template($find);
  if ($template === FALSE) {
    return FALSE;
  }
  $template = DrushFieldsReadFormatter::templatePrepare($template, $find);

  $header = drush_fields_yn(drush_get_option('header', TRUE), TRUE);
  if (is_null($header)) {
    return FALSE;
  }

  $result = $q->execute();
  $count = 0;
  while ($r = $result->fetchAssoc()) {

    // If any of php conditions failed ...
    $cond = TRUE;
    foreach ($php_conditions as $c) {
      list($var, $op, $data) = $c;
      $value = &$r[$var];
      if (!$op($value, $data)) {
        $cond = FALSE;
        break;
      }
    }
    if (!$cond) {
      continue;
    }

    // Print header only when there is going to be some actual results.
    if ($header && $count == 0) {
      $tmp = NULL;
      drush_print(drush_fields_find_template_apply($template, $tmp));
    }
    drush_print(drush_fields_find_template_apply($template, $r));
    $count++;
  }

  if ($count == 0) {
    drush_set_error(dt('Nothing found.'));
  }

}

/**
 * Zip template and a db record and produce formatted output.
 *
 * @param array $template
 *   Prepared template.
 * @param array|NULL $record
 *   NULL - header.
 *   Array - row.
 *
 * @return string
 *   Return formatted line.
 */
function drush_fields_find_template_apply(array &$template, &$record) {

  $line = '';
  foreach ($template as $p) {
    switch (gettype($p)) {

      case 'string':
        $line .= $p;
        break;

      case 'object':
        if (is_null($record)) {
          $line .= $p->head();
        }
        else {
          $line .= $p->body($record['delta'], $record);
        }
        break;

    }
  }
  return $line;

}

/**
 * Prepares integer matching.
 *
 * Verify operator and its arguemnt(s), prepare it, and return filter info.
 *
 * @param string $op
 *   Operator like '<' or '>' etc.
 * @param string $value
 *   Operator argument.
 *
 * @return array|bool
 *   Array with two subarrays (db filter and php filter) - success.
 *   FALSE - failure.
 */
function drush_fields_find_integer($op, $value) {

  $ops1 = array('=', '<', '>', '<>', '<=', '>=');
  if (in_array($op, $ops1)) {
    if (!preg_match('/^[+-]?\d+$/', $value)) {
      return drush_set_error(dt('\'!value\': Must be integer.', array('!value' => $value)));
    }
    $int = (int) $value;
    $cond = array($int, $op);
    return array($cond, array());
  }

  $ops2 = array('in', 'not-in');
  if (in_array($op, $ops2)) {
    if (!preg_match('/^[+-]?\d+(,[+-]?\d+)+$/', $value)) {
      return drush_set_error(dt('\'!value\': Must be list of integers, comma-separated.', array('!value' => $value)));
    }
    $ints = explode(',', $value);
    foreach ($ints as &$i) {
      $i = (int) $i;
    }
    $ints = array_unique($ints, SORT_NUMERIC);
    $op = preg_replace('/-/', ' ', $op);
    $cond = array($ints, $op);
    return array($cond, array());
  }

  $ops3 = array('between', 'not-between');
  if (in_array($op, $ops3)) {
    if (!preg_match('/^([+-]?\d+)-([+-]?\d+)$/', $value, $match)) {
      return drush_set_error(dt('\'!value\': Must be integer range, min-max.', array('!value' => $value)));
    }
    $min = (int) $match[1];
    $max = (int) $match[2];
    if ($min >= $max) {
      return drush_set_error(dt('!range: Expected: min < max.', array('!range' => $value)));
    }
    $range = array($min, $max);
    $op = preg_replace('/-/', ' ', $op);
    $cond = array($range, $op);
    return array($cond, array());
  }

  $ops = array_merge($ops1, $ops2, $ops3);
  $ops = implode(' ', $ops);
  return drush_set_error(dt('\'!op\': Invalid integer operation. Expected: !ops', array('!op' => $op, '!ops' => $ops)));

}

/**
 * Prepares float matching.
 *
 * Verify operator and its arguemnt(s), prepare it, and return filter info.
 *
 * @param string $op
 *   Operator like '<' or '>' etc.
 * @param string $value
 *   Operator argument.
 *
 * @return array|bool
 *   Array with two subarrays (db filter and php filter) - success.
 *   FALSE - failure.
 */
function drush_fields_find_float($op, $value) {

  $ops1 = array('<', '>', '<=', '>=');
  if (in_array($op, $ops1)) {
    if (!is_numeric($value)) {
      return drush_set_error(dt('\'!value\': Must be float.', array('!value' => $value)));
    }
    $float = (float) $value;
    $cond = array($float, $op);
    return array($cond, array());
  }

  $ops2 = array('between', 'not-between');
  if (in_array($op, $ops2)) {
    if (!preg_match('/^(.+)-(.+)$/', $value, $match)) {
      return drush_set_error(dt('\'!value\': Must be float range, min-max.', array('!value' => $value)));
    }
    if (!is_numeric($match[1])) {
      return drush_set_error(dt('\'!min\': Must be float.', array('!min' => $match[1])));
    }
    if (!is_numeric($match[2])) {
      return drush_set_error(dt('\'!max\': Must be float.', array('!max' => $match[2])));
    }
    $min = (float) $match[1];
    $max = (float) $match[2];
    if ($min >= $max) {
      return drush_set_error(dt('!range: Expected: min < max.', array('!range' => $value)));
    }
    $range = array($min, $max);
    $op = preg_replace('/-/', ' ', $op);
    $cond = array($range, $op);
    return array($cond, array());
  }

  $ops = array_merge($ops1, $ops2);
  $ops = implode(' ', $ops);
  return drush_set_error(dt('\'!op\': Invalid float operation. Expected: !ops', array('!op' => $op, '!ops' => $ops)));

}

/**
 * Prepares string matching.
 *
 * Verify operator and its arguemnt(s), prepare it, and return filter info.
 *
 * @param string $op
 *   Operator like '=' or '<>' etc.
 * @param string $value
 *   Operator argument.
 *
 * @return array|bool
 *   Array with two subarrays (db filter and php filter) - success.
 *   FALSE - failure.
 */
function drush_fields_find_string($op, $value) {

  $ops1 = array('=', '<>', 'like', 'not-like');
  if (in_array($op, $ops1)) {
    $str = (string) $value;
    $op = preg_replace('/-/', ' ', $op);
    $cond = array($str, $op);
    return array($cond, array());
  }

  $ops2 = array('~', '!~');
  if (in_array($op, $ops2)) {
    $rx = (string) $value;
    // Do not suppress by @.
    if (preg_match($rx, '') === FALSE) {
      return drush_set_error(dt('--rx: Invalid regular expression.'));
    }
    $operator_2_callback = array(
      '~' => 'drush_fields_find_string_rx',
      '!~' => 'drush_fields_find_string_not_rx',
    );
    $op = $operator_2_callback[$op];
    $cond = array($op, $rx);
    return array(array(), $cond);
  }

  $ops = array_merge($ops1, $ops2);
  $ops = implode(' ', $ops);
  return drush_set_error(dt('\'!op\': Invalid string operation. Expected: !ops', array('!op' => $op, '!ops' => $ops)));

}

/**
 * Perform positive rx match.
 *
 * @param string $val
 *   String to check.
 * @param string $rx
 *   Regular expression for checking.
 *
 * @return bool
 *   TRUE - yes match.
 *   FALSE - no match.
 */
function drush_fields_find_string_rx(&$val, &$rx) {
  return preg_match($rx, $val);
}

/**
 * Perform negative rx match.
 *
 * @param string $val
 *   String to check.
 * @param string $rx
 *   Regular expression for checking.
 *
 * @return bool
 *   TRUE - no match.
 *   FALSE - yes match.
 */
function drush_fields_find_string_not_rx(&$val, &$rx) {
  return !preg_match($rx, $val);
}
