<?php

/**
 * @file
 * Command implementations for Term Reference field.
 */

class DrushFieldsFieldTaxonomyTermReference extends DrushFields {

  /**
   * Field-specific command description implementation.
   *
   * @return array
   *   Field-specific command description.
   */
  static public function updateCmd() {

    return array(
      'examples' => array(
        'drush fields-update node 1234 field_tags --op=set --value=\'1,2,3\''
        => 'Set field_tags to refer to terms with id 1, 2 and 3 of node 1234.',
      ),
    );

  }

  /**
   * Field-specific implementation of update preparation.
   *
   * @param string $values
   *   Command-line argument --value value.
   *
   * @return array
   *   Field-specific values to set/add.
   */
  protected function updatePrepare($values) {

    $vocabularys = array();
    foreach ($this->general['settings']['allowed_values'] as $v) {
      $v = taxonomy_vocabulary_machine_name_load($v);
      $v = entity_metadata_wrapper('taxonomy_vocabulary', $v);
      $v = $v->getIdentifier();
      $vocabularys[$v] = TRUE;
    }

    // Init duplication check.
    $data = array();
    foreach ($this->values as $t) {
      $t = entity_metadata_wrapper('taxonomy_term', $t);
      $data[] = (int) $t->getIdentifier();
    }
    drush_fields_dups($data);

    $ret = array();

    $values = explode(',', $values);
    $values = preg_replace('/\s+/', '', $values);
    foreach ($values as $tid) {
      if (!preg_match('/^\d+$/', $tid)) {
        return $this->errorSet(dt('!tid: Invalid Term ID, integer expected.', array('!tid' => $tid)));
      }
      $tid = (int) $tid;
      $term = taxonomy_term_load($tid);
      if (empty($term)) {
        $this->errorSet(dt('!tid: No such term.', array('!tid' => $tid)));
        $terms = array();
        foreach (taxonomy_get_tree(1, 0, NULL, TRUE) as $t) {
          $terms[] = $t->tid . ': ' . $t->name;
        }
        $this->errorSetOptions($terms);
        return FALSE;
      }
      $term = entity_metadata_wrapper('taxonomy_term', $term);
      $vocabulary = $term->vocabulary->raw();
      if (!$vocabularys[$vocabulary]) {
        return $this->errorSet(dt('!tid: Term belongs to non-allowed vocabulary.', array('!tid' => $tid)));
      }

      // Skip dups.
      if (drush_fields_dups($tid) === FALSE) {
        continue;
      }

      $ret[] = $term->value();
    }

    return $ret;

  }

  /**
   * Field-specific 'read' template.
   *
   * @return string
   *   Return template string.
   */
  public function defaultTemplate() {
    return '{%6d:delta} {%6d:tid} {%s:name}';
  }

  /**
   * Field-specific 'read' keys.
   *
   * @return array
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {

    return array(
      'delta' => new DrushFieldsReadFormatterDelta(),
      'tid' => new DrushFieldsReadFormatterTaxonomyTerm('tid', dt('TID'), dt('Taxonomy Term ID.')),
      'name' => new DrushFieldsReadFormatterTaxonomyTerm('name', dt('Name'), dt('Taxonomy Term Name.')),
    );

  }

}
