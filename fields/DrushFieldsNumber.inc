<?php

/**
 * @file
 * Command implementations for Number* fields.
 */

class DrushFieldsFieldNumberDecimal extends DrushFieldsNumber {}
class DrushFieldsFieldNumberFloat extends DrushFieldsNumber {}
class DrushFieldsFieldNumberInteger extends DrushFieldsNumber {}

class DrushFieldsNumber extends DrushFields {

  /**
   * Field-specific command description implementation.
   *
   * @return array
   *   Field-specific command description.
   */
  static public function updateCmd() {

    return array(
      'value' => array(
        'Multiple numeric values are separated by comma. (Decimal, Float, Integer)',
      ),
      'examples' => array(
        'drush fields-update node 1234 field_count --op=set --value=55'
        => 'Set 55 to field_count of node 1234.',
        'drush fields-update node 1234 field_lotto --op=add --value="10,17,63'
        => 'Add values 10, 17 and 63 to field field_lotto.',
        'drush fields-update node 1234 field_money --op=set --value=9.99'
        => 'Set 9.99 to field_money of node 1234.',
        'drush fields-update node 1234 field_measure --op=set --value=44.22'
        => 'Set 44.22 to field_measure of node 1234.',
      ),
    );

  }

  /**
   * Field-specific implementation of update preparation.
   *
   * @param string $values
   *   Command-line argument --value value.
   *
   * @return array
   *   Field-specific values to set/add.
   */
  protected function updatePrepare($values) {

    $values = preg_replace('/\s+/', '', $values);
    $values = explode(',', $values);
    $list = array();
    foreach ($values as $v) {

      switch ($this->fieldType) {
        case 'number_decimal':
          if (!is_numeric($v)) {
            return $this->errorSet(dt('--value: "!v" must be decimal.', array('!v' => $v)));
          }
          break;

        case 'number_float':
          if (!preg_match('/^[+-]?(\d+|\.\d+|\d+\.\d+)$/', $v)) {
            return $this->errorSet(dt('--value: "!v" must be float.', array('!v' => $v)));
          }
          $v = (float) $v;
          break;

        case 'number_integer':
          if (!preg_match('/^[+-]?\d+$/', $v)) {
            return $this->errorSet(dt('--value: "!v" must be integer.', array('!v' => $v)));
          }
          $v = (int) $v;
          break;
      }

      $list[] = $v;
    }

    return $list;

  }

  /**
   * Field-specific 'read' template.
   *
   * @return string
   *   Return template string.
   */
  public function defaultTemplate() {
    switch ($this->fieldType) {
      case 'number_decimal':
        return '{%6d:delta} {%12s:dec}';

      case 'number_float':
        return '{%6d:delta} {%12s:float}';

      case 'number_integer':
        return '{%6d:delta} {%9d:int}';
    }
  }

  /**
   * Field-specific 'read' keys.
   *
   * @return array
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {

    switch ($this->fieldType) {
      case 'number_decimal':
        $ret = array(
          'delta' => new DrushFieldsReadFormatterDelta(),
          'dec' => new DrushFieldsReadFormatterValue(dt('Decimal'), dt('Decimal value.')),
        );
        return $ret;

      case 'number_float':
        $ret = array(
          'delta' => new DrushFieldsReadFormatterDelta(),
          'float' => new DrushFieldsReadFormatterValue(dt('Float'), dt('Floating-point value.')),
        );
        return $ret;

      case 'number_integer':
        $ret = array(
          'delta' => new DrushFieldsReadFormatterDelta(),
          'int' => new DrushFieldsReadFormatterValue(dt('Integer'), dt('Integer value.')),
        );
        return $ret;
    }

  }

}
