<?php

/**
 * @file
 * Common functions for node-title access and manipulation.
 */

/**
 * Check NID and fetch wrapper.
 *
 * Check if user specified good NID and return entity wrapper. In case of error
 * print out message and return FALSE.
 *
 * @param string $nid
 *   Node ID.
 *
 * @return object|bool
 *   Return Entity Wrapper object or FALSE in case of error.
 */
function drush_fields_node_title_wrapper_by_nid($nid) {

  if (!is_numeric($nid)) {
    return drush_set_error(dt('NID (!nid): Must be numeric.', array('!nid' => $nid)));
  }

  $node = node_load($nid);
  if (empty($node)) {
    return drush_set_error(dt('NID (!nid): No such node.', array('!nid' => $nid)));
  }

  $wrapper = entity_metadata_wrapper('node', $node);

  return $wrapper;

}
