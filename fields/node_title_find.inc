<?php

/**
 * @file
 * Node title find functionality.
 */

/**
 * Provide Drush commands this "plugin" can handle.
 *
 * @return array
 *   Return an array of Drush commands.
 */
function drush_fields_node_title_find_cmd() {

  return array(

    'fields-node-title-find' => array(
      'description' => 'Find nodes by title.',
      'callback' => 'drush_fields_node_title_find',
      'examples' => array(
        'drush fields-node-title-find --rx=\'/\d/\''
        => 'Find nodes which contain a number.',
        'drush fields-node-title-find --rx=\'/./\''
        => 'List all node titles.',
        'drush fields-node-title-find --rx=\'/[A-Z]{3,}/\''
        => 'Find CAPS abuse.',
      ),
      'options' => array(
        'rx' => 'Required. Perl Compatible Regular Expression (PHP PCRE). See preg_replace(). Must specify delimiters.',
      ),
      /* Have to be able to use --user option. */
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
      'core' => array('7'),
      'drupal dependencies' => array(
        'entity',
        'node',
      ),
    ),

  );

}

/**
 * Perform actual title find.
 *
 * Print stdout Node ID and Node title. Uses PHP PCRE.
 */
function drush_fields_node_title_find() {

  $rx = drush_get_option('rx', NULL);
  if (empty($rx) && $rx !== '0') {
    return drush_set_error(dt('--rx: Required.'));
  }

  // Do not suppress by @.
  if (preg_match($rx, '') === FALSE) {
    return drush_set_error(dt('--rx: Invalid regular expression.'));
  }

  $result = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->orderby('nid')
    ->execute();

  $count = 0;
  while ($n = $result->fetchAssoc()) {
    $nid = $n['nid'];
    $title = $n['title'];
    // Perform title check first, since it is quicker.
    if (!preg_match($rx, $title)) {
      continue;
    }
    if (!drush_fields_access('view', 'node', node_load($nid))) {
      continue;
    }
    $line = "$nid: $title";
    drush_print($line);
    $count = $count + 1;
  }

  drush_print(dt('Total !count matches.', array('!count' => $count)));

}
