<?php

/**
 * @file
 * Handle field data update.
 */

/**
 * Provide Drush commands this "plugin" can handle.
 *
 * @return array
 *   Return an array of Drush commands.
 */
function drush_fields_update_cmd() {

  drush_fields_require_once_classes();

  /* Make description and value as array so that field-specific functions can
  easily add their own info. Later implode to return proper data. */

  $item = array(

    'fields-update' => array(
      'callback' => 'drush_fields_update',
      'arguments' => array(
        'entity type machine name' => 'Required. Example: node',
        'entity id' => 'Required. Example: 1234',
        'field machine name' => 'Required. Example: field_pdf',
      ),
      'required-arguments' => 3,
      /* Have to be able to use --user option. */
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
      'core' => array('7'),
      'drupal dependencies' => array(
        'entity',
        // Appropriate field module must be enabled.
      ),
    ),

  );

  $description = array(
    'Update field data.',
  );
  $examples = array(
    'drush fields-update node 1 field_geo --op=import --value=/tmp/location.xml'
    => 'Import /tmp/location.xml into field_geo of node 1.',
    'cat /tmp/location.xml | drush fields-update node 1 field_geo --op=import --value=-.xml -y'
    => 'Import /tmp/location.xml into field_geo of node 1. Need to supply -y.',
  );
  $value = array(
    'Requied.',
  );
  $options = array(
    'op' => 'Required. \'add\', \'set\' or \'import\' data to multi-value field.',
    'save' => drush_fields_save_options_description(),
  );

  require_once '_commands.inc';
  // We dont have field_info_fields() due to low-level bootstrap.
  foreach (drush_fields_supported() as $f) {

    $class = strtr($f, '_', ' ');
    $class = ucwords($class);
    $class = strtr($class, array(' ' => ''));
    $class = "DrushFieldsField$class";
    if (!class_exists($class)) {
      continue;
    }
    $cmd = $class::updateCmd();

    $e = &$cmd['examples'];
    if (!empty($e)) {
      $examples = array_merge($examples, $e);
    }

    $o = &$cmd['options'];
    if (!empty($o)) {
      $options = array_merge($options, $o);
    }

    $d = &$cmd['description'];
    switch (gettype($d)) {
      case 'string':
        $data = array($d);
        break;

      case 'array':
        $data = $d;
        break;

      default:
        $data = array();
    }
    foreach ($data as $d) {
      if (!in_array($d, $description)) {
        $description[] = $d;
      }
    }

    $v = &$cmd['value'];
    switch (gettype($v)) {
      case 'string':
        $data = array($v);
        break;

      case 'array':
        $data = $v;
        break;

      default:
        $data = array();
    }
    foreach ($data as $v) {
      if (!in_array($v, $value)) {
        $value[] = $v;
      }
    }

  }

  $item['fields-update']['description'] = implode("\n", $description);
  $item['fields-update']['examples'] = $examples;
  $options['value'] = implode("\n", $value);
  $item['fields-update']['options'] = $options;

  return $item;

}

/**
 * Implements field element general update. Performs checks.
 *
 * @param string $entity_type
 *   Entity machine name. Examples: node, profile2.
 * @param string|int $entity_id
 *   Entity ID (nid, uid, etc). Examples: 1, 10, 100, 31415.
 * @param string $field_name
 *   Field machine name. Example: field_measure.
 */
function drush_fields_update($entity_type = NULL, $entity_id = NULL, $field_name = NULL) {

  drush_fields_require_once_classes();

  $field = new DrushFields($entity_type, $entity_id, $field_name);
  if ($field->errorGet() === TRUE) {
    return FALSE;
  }

  $values = drush_get_option('value', NULL);
  if (!is_string($values) || strlen($values) < 1) {
    return drush_set_error(dt('--value is required.'));
  }

  $op = drush_fields_arg_op();
  if ($op === FALSE) {
    return FALSE;
  }

  // Universal / Field-specific approach.
  if ($op != 'import') {
    $field = $field->extend();
    if (is_null($field)) {
      return drush_set_error(dt('This field type is not supported. Try import.'));
    }
  }

  $save_method = drush_fields_arg_save();
  if ($save_method === FALSE) {
    return FALSE;
  }

  switch ($op) {
    case 'add':
    case 'set':
      $field->update($op, $save_method, $values);
      break;

    case 'import':
      $field->import($save_method, $values);
      break;

    default:
      return drush_set_error(dt('!f: Internal error. Invalid op \'!op\'.', array('!f' => __FUNCTION__, '!op' => $op)));
  }
}

/**
 * Process --op=add and --op=set options.
 *
 * @return string|bool
 *   Returns 'add' or 'set' or FALSE on error.
 */
function drush_fields_arg_op() {

  $options = array(
    'add',
    'set',
    'import',
  );
  $op = drush_get_option('op', NULL);
  if (!is_string($op) || !in_array($op, $options)) {
    return drush_set_error(dt('--op: Choose one: !op.', array('!op' => implode(', ', $options))));
  }

  return $op;
}
