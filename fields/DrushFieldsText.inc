<?php

/**
 * @file
 * Command implementations for Text* fields.
 */

class DrushFieldsFieldText extends DrushFieldsText {}
class DrushFieldsFieldTextLong extends DrushFieldsText {}
class DrushFieldsFieldTextWithSummary extends DrushFieldsText {}

class DrushFieldsText extends DrushFields {

  /**
   * Field-specific command description implementation.
   *
   * @return array
   *   Field-specific command description.
   */
  static public function updateCmd() {

    return array(
      'value' => array(
        drush_fields_arg_data_desc() . ' (Long Text, Text with summary)',
      ),
      'options' => array(
        'delimiter' => 'Optional. Delmiter is used to separate multiple values. (Text)',
        'filter' => 'Required. Text format type. Example: full_html, plain_text. (Text with summary)',
      ),
      'examples' => array(
        'drush fields-update node 1234 field_rfc --op=set --value=\'http://www.example.com/rfc1234.html\' --filter=full_html'
        => 'Set field_rfc to contents of \'http://...\' of node 1234.',
        'drush fields-update node 1234 field_desc --op=set --value=\'-\' --filter=text_plain'
        => 'Set field_desc contents from stdin of node 1234.',
        'drush fields-update node 1234 field_name --op=set --value=\'John Doe,Eenie Weenie\' --delimiter=\',\''
        => 'Set field_name to \'John Doe\' and \'Eenie Weenie\' of node 1234.',
        'drush fields-update node 1234 field_code --op=set --value=\'/tmp/debug.php\''
        => 'Set field_code to contents of the file \'/tmp/debug.php\' of node 1234.',
      ),
    );

  }

  /**
   * Field-specific implementation of update preparation.
   *
   * @param string $values
   *   Command-line argument --value value.
   *
   * @return array
   *   Field-specific values to set/add.
   */
  protected function updatePrepare($values) {

    /* Though these fields bear no common code in implementation,
    they belong to same "family" and have short implementation. */

    switch ($this->fieldType) {

      case 'text':
        $delimiter = drush_get_option('delimiter', NULL);
        if (is_string($delimiter) && strlen($delimiter) > 0) {
          $values = explode($delimiter, $values);
        }
        else {
          $values = array($values);
        }
        break;

      case 'text_long':
        $values = drush_fields_arg_data($values);
        if ($values === FALSE) {
          return FALSE;
        }
        $values = array($values);
        break;

      case 'text_with_summary':
        $values = drush_fields_arg_data($values);
        if ($values === FALSE) {
          return FALSE;
        }

        $filter = $this->argFilter();
        if ($filter === FALSE) {
          $this->error = TRUE;
          return FALSE;
        }

        $values = array(
          array(
            'value' => $values,
            'format' => $filter,
          ),
        );
        break;

    }

    return $values;
  }

  /**
   * Process --filter.
   *
   * Check for possible filter choices and access to them.
   *
   * @return string|bool
   *   Return filter machine name.
   *   Return FALSE on error.
   */
  private function argFilter() {

    $filter = drush_get_option('filter', NULL);
    if (empty($filter) || !is_string($filter)) {
      return $this->errorSet(dt('--filter: Must be text filter machine name.'));
    }

    if (!filter_format_exists($filter)) {
      $this->errorSet(dt('--filter=\'!filter\': No such filter.', array('!filter' => $filter)));
      drush_fields_options(array_keys(filter_formats()));
      return FALSE;
    }

    $uid = $GLOBALS['user']->uid;
    if ($uid == 0 || $uid == 1) {
      return $filter;
    }

    if (!filter_access($filter)) {
      return $this->errorSet(dt('--filter: You dont have permission to use this filter.'));
    }

    return $filter;

  }

  /**
   * Field-specific 'read' template.
   *
   * @return string
   *   Return template string.
   */
  public function defaultTemplate() {
    switch ($this->fieldType) {
      case 'text':
      case 'text_long':
        return '{%6d:delta} {%s:text}';

      case 'text_with_summary':
        return '{%6d:delta} {%-10s:format} {%s:text}';
    }
  }

  /**
   * Field-specific 'read' keys.
   *
   * @return array
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {

    switch ($this->fieldType) {
      case 'text':
      case 'text_long':
        $ret = array(
          'delta' => new DrushFieldsReadFormatterDelta(),
          'text' => new DrushFieldsReadFormatterValue(dt('Text'), dt('Text value, in plain text.')),
          'base64' => new DrushFieldsReadFormatterBase64(dt('Base64'), dt('Text value, in base64.')),
        );
        return $ret;

      case 'text_with_summary':
        $ret = array(
          'delta' => new DrushFieldsReadFormatterDelta(),
          'text' => new DrushFieldsReadFormatterValue(dt('Text'), dt('Text value, in plain text.'), array('value')),
          'base64' => new DrushFieldsReadFormatterBase64(dt('Base64'), dt('Text value, in base64.'), array('value')),
          'format' => new DrushFieldsReadFormatterValue(dt('Format'), dt('Format type.'), array('format')),
        );
        return $ret;

    }

  }

}
