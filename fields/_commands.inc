<?php

/**
 * @file
 * This file serves only one purpose: provide command list.
 */

/**
 * Provide list of available commands.
 *
 * For each entry xxx there must be file "fields/xxx.inc" and function
 * "drush_fields_xxx_cmd".
 *
 * @return array
 *   Array contains list of strings.
 */
function drush_fields_commands() {

  return array(

    'count',
    'del',
    'read',
    'update',
    'info',
    'find',

    'node_title_find',
    'node_title_read',
    'node_title_replace',
    'node_title_set',

  );

}

/**
 * Helps including all classes.
 */
function drush_fields_require_once_classes() {

  require_once 'DrushFields.inc';
  require_once 'DrushFieldsReadFormatter.inc';

  require_once 'DrushFieldsFile.inc';
  require_once 'DrushFieldsDate.inc';
  require_once 'DrushFieldsList.inc';
  require_once 'DrushFieldsNumber.inc';
  require_once 'DrushFieldsText.inc';

  require_once 'DrushFieldsEmail.inc';
  require_once 'DrushFieldsEntityreference.inc';
  require_once 'DrushFieldsLinkField.inc';
  require_once 'DrushFieldsTaxonomyTermReference.inc';

}

/**
 * List of supporteed fields.
 *
 * @return array
 *   Returns array of strings of field types.
 */
function drush_fields_supported() {
  return array(
    'date',
    'datestamp',
    'datetime',
    'email',
    'entityreference',
    'file',
    'image',
    'link_field',
    'list_boolean',
    'list_float',
    'list_integer',
    'list_text',
    'number_decimal',
    'number_float',
    'number_integer',
    'taxonomy_term_reference',
    'text',
    'text_long',
    'text_with_summary',
  );
}
