<?php

/**
 * @file
 * Read formatters, base class and specific implementations.
 */

/**
 * General 'read' placeholder formatter class.
 */
class DrushFieldsReadFormatter {

  protected $name;
  protected $desc;
  protected $sub;
  protected $sprintf;

  /**
   * Prepare template.
   *
   * Cut template into pieces: strings and formatter objects which can be
   * executed in sequence to construct an output line.
   *
   * @param string $template
   *   Template according to which output is formatted.
   * @param object $obj
   *   Object containing keys.
   *
   * @return array|bool
   *   Array of strings and formatter objects in sequence.
   *   FALSE - No formatter found.
   */
  static public function templatePrepare($template, $obj) {

    $ret = array();
    $pieces = preg_split('/(\{[^}]+\})/', $template, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
    foreach ($pieces as $p) {
      if (preg_match('/^\{.+\}$/', $p, $match)) {
        $formatter = DrushFieldsReadFormatter::placeholderPrepare($p, $obj);
        if ($formatter === FALSE) {
          return FALSE;
        }
        $ret[] = $formatter;
      }
      else {
        $ret[] = $p;
      }
    }

    return $ret;

  }

  /**
   * Convert one placeholder {format:key} into respective formatter object.
   *
   * Support {fmt:key} and {fmt:~key|key|key...}. Fmt is printf()-style
   * placeholder. Single key represents constructed formatter in field.
   * '~key|key|key...' represents universal formatting, where keys are used to
   * travel data structure.
   *
   * @param string $placeholder
   *   Textual placeholder in template in form {F:K}.
   * @param object $field
   *   The field object.
   *
   * @return object|bool
   *   Formatter object.
   *   FALSE on error.
   */
  static protected function placeholderPrepare($placeholder, $field) {

    $placeholder = preg_replace('/^\{(.*)\}$/', '$1', $placeholder);

    $tmp = explode(':', $placeholder);
    if (count($tmp) != 2) {
      return drush_set_error(dt('Placeholder {!p} is invalid. Expected {F:K} where F=printf-style-formatter and K=Key or K=~key|key|key...', array('!p' => $placeholder)));
    }
    list($fmt, $key) = $tmp;

    // Gross sprintf() placeholder syntax check.
    if (!preg_match('/^%\+?( |0|\'.)?-?\d*(\.\d+)?[bcdeEfFgGosuxX]$/', $fmt)) {
      return drush_set_error(dt('Printf-style-formatter \'!fmt\' is invalid. Example: \'%s\'. Check PHP sprintf() page.', array('!fmt' => $fmt)));
    }

    $keys = $field->keysGet();

    $rx = '/^[a-z0-9_-]+$/';
    if (preg_match($rx, $key)) {
      if (!array_key_exists($key, $keys)) {
        drush_set_error(dt('Format: Unknown key \'!key\'.', array('!key' => $key)));
        drush_fields_keys($field);
        return FALSE;
      }
      $formatter = $keys[$key];
      $formatter->formatterSet($fmt);
      return $formatter;
    }

    $rx = '/^~([a-z0-9|_-]*)$/';
    if (preg_match($rx, $key, $match)) {
      $key = $match[1];
      if (preg_match('/(^\|)|(\|\|)|(\|$)/', $key)) {
        return drush_set_error(dt('Key \'!key\' may not begin with pipe, may not end with pipe, may not contain more than one pipes in sequence.', array('!key' => $key)));
      }
      $sub = strlen($key) ? explode('|', $key) : array();
      $formatter = new DrushFieldsReadFormatterValue(dt('Value'), dt('Raw value.'), $sub);
      $formatter->formatterSet($fmt);
      return $formatter;
    }

    return drush_set_error(dt('Key \'!key\' is invalid. Must match regex: \'!rx\'.', array('!key' => $key, '!rx' => $rx)));

  }

  /**
   * Use printf()-style formatter for constructing output.
   *
   * @param string $fmt
   *   Set $fmt to default printf()-style output ormatter.
   */
  private function formatterSet($fmt) {
    $this->sprintf = $fmt;
  }

  /**
   * Constructor for general 'read' placeholder formatter.
   *
   * @param string $name
   *   Short column name for key.
   * @param string $desc
   *   Description of key.
   * @param array|NULL $sub
   *   Array of sub-array keys. If values contains data structure of nested
   *   array, $sub specifies path to data element.
   *   Example: array('metadata', 'width').
   */
  public function __construct($name = NULL, $desc = NULL, $sub = NULL) {
    $this->name = $name;
    $this->desc = $desc;
    $this->sub = is_array($sub) ? $sub : array();
  }

  /**
   * Return formatter column name.
   *
   * @return string
   *   Short name.
   */
  public function name() {
    return $this->name;
  }

  /**
   * Return formatter description.
   *
   * @return string
   *   Description.
   */
  public function description() {
    return $this->desc;
  }

  /**
   * Return header with printf()-style formatting.
   *
   * Note that header is tried to convert same style as column data, but that
   * is approximate and leads to sub-optimal results in certain cases like
   * '%2.2f'.
   *
   * @return string
   *   Return header element.
   */
  public function head() {
    $fmt = preg_replace('/[a-zA-Z]$/', 's', $this->sprintf);
    $head = sprintf($fmt, $this->name);
    return $head;
  }

  /**
   * Return value with printf()-style formatting.
   *
   * @return string
   *   Return body element.
   */
  public function body($delta, &$value, array &$general) {
    $data = $this->format($delta, $value, $general);
    $data = sprintf($this->sprintf, $data);
    return $data;
  }

  /**
   * Perform actual formatting.
   *
   * This is placeholder method for sub-classes.
   *
   * @param int $delta
   *   Delta of field element.
   * @param mixed $value
   *   Value of field element, baesd on $wrapper->field->value().
   * @param array $general
   *   General field info from field_info_field().
   *
   * @return string
   *   String to be printed to user.
   */
  protected function format($delta, &$value, array &$general) {
    return '';
  }

  /**
   * Read value from value structure according to $sub.
   *
   * Following conditions apply:
   * - Can travel through arrays/objects.
   * - If cannot reach specified data, return ''.
   * - If $value is array(), then return the value.
   * - If resulting element is not usable data (bool,float,int,str) then print
   *   '(type)'.
   *
   * @param mixed $value
   *   Value of field element, based on $wrapper->field->value().
   *
   * @return mixed
   *   Return data from value according to $sub.
   */
  protected function sub(&$value) {

    $v = &$value;
    foreach ($this->sub as $s) {
      switch (gettype($v)) {

        case 'array':
          $v = &$v[$s];
          break;

        case 'object':
          $v = &$v->{$s};
          break;

        default:
          return '';
      }
    }

    switch (gettype($v)) {
      case 'boolean':
      case 'float':
      case 'integer':
      case 'string':
        return $v;

      default:
        return '(' . gettype($v) . ')';
    }
  }

}

class DrushFieldsReadFormatterDelta extends DrushFieldsReadFormatter {

  /**
   * Constructor.
   *
   * @param string $name
   *   Name.
   * @param string $desc
   *   Description.
   */
  public function __construct($name = NULL, $desc = NULL) {
    if (is_null($name)) {
      $name = dt('Delta');
    }
    if (is_null($desc)) {
      $desc = dt('Field element delta.');
    }
    parent::__construct($name, $desc);
  }

  /**
   * Formatting.
   *
   * @param int $delta
   *   Delta.
   *
   * @return string
   *   Formatted value.
   */
  protected function format($delta) {
    return (string) (int) $delta;
  }

}

class DrushFieldsReadFormatterValue extends DrushFieldsReadFormatter {

  /**
   * Constructor.
   *
   * @param string $name
   *   Name.
   * @param string $desc
   *   Description.
   * @param array|NULL $sub
   *   Sub.
   */
  public function __construct($name = NULL, $desc = NULL, $sub = NULL) {
    if (is_null($name)) {
      $name = dt('Value');
    }
    if (is_null($desc)) {
      $desc = dt('Field data value.');
    }
    parent::__construct($name, $desc, $sub);
  }

  /**
   * Formatting.
   *
   * @param int $delta
   *   Delta.
   * @param mixed $value
   *   Value.
   *
   * @return string
   *   Formatted value.
   */
  protected function format($delta, &$value) {
    return $this->sub($value);
  }

}

class DrushFieldsReadFormatterBase64 extends DrushFieldsReadFormatter {

  /**
   * Constructor.
   *
   * @param string $name
   *   Name.
   * @param string $desc
   *   Description.
   * @param array|NULL $sub
   *   Sub.
   */
  public function __construct($name = NULL, $desc = NULL, $sub = NULL) {
    if (is_null($name)) {
      $name = dt('Base64');
    }
    if (is_null($desc)) {
      $desc = dt('Field data value in base64 format.');
    }
    parent::__construct($name, $desc, $sub);
  }

  /**
   * Formatting.
   *
   * @param int $delta
   *   Delta.
   * @param mixed $value
   *   Value.
   *
   * @return string
   *   Formatted value.
   */
  protected function format($delta, &$value) {
    return base64_encode($this->sub($value));
  }

}

class DrushFieldsReadFormatterDateTime extends DrushFieldsReadFormatter {

  protected $format;

  /**
   * Constructor.
   *
   * @param string $format
   *   Format: date->format().
   * @param string $name
   *   Name.
   * @param string $desc
   *   Description.
   * @param array|NULL $sub
   *   Sub.
   */
  public function __construct($format, $name = NULL, $desc = NULL, $sub = NULL) {
    if (is_null($name)) {
      $name = dt('DateTime');
    }
    if (is_null($desc)) {
      $desc = dt('Date and Time.');
    }
    parent::__construct($name, $desc, $sub);
    $this->format = $format;
  }

  /**
   * Formatting.
   *
   * It works with date structure (use timezone* info)
   * and also with plain values (ex: file->created).
   *
   * @param int $delta
   *   Delta.
   * @param mixed $value
   *   Value.
   *
   * @return string
   *   Formatted value.
   */
  protected function format($delta, &$value) {
    $tz = empty($value['timezone_db']) ? drupal_get_user_timezone() : $value['timezone_db'];
    $d = drush_fields_date($this->sub($value), $tz);
    $tz = empty($value['timezone']) ? drupal_get_user_timezone() : $value['timezone'];
    $tz = new DateTimeZone($tz);
    $d->setTimezone($tz);
    return $d->format($this->format);
  }

}

class DrushFieldsReadFormatterDateOffset extends DrushFieldsReadFormatter {

  protected $format;

  /**
   * Constructor.
   *
   * @param string $format
   *   Format: s, hm.
   * @param string $name
   *   Name.
   * @param string $desc
   *   Description.
   * @param array|NULL $sub
   *   Sub.
   */
  public function __construct($format, $name = NULL, $desc = NULL, $sub = NULL) {
    if (is_null($name)) {
      $name = dt('Offset');
    }
    if (is_null($desc)) {
      $desc = dt('Date/Time Offset.');
    }
    parent::__construct($name, $desc, $sub);
    $this->format = $format;
  }

  /**
   * Formatting.
   *
   * @param int $delta
   *   Delta.
   * @param mixed $value
   *   Value.
   *
   * @return string
   *   Formatted value.
   */
  protected function format($delta, &$value) {
    $off = $this->sub($value);
    switch ($this->format) {
      case 's':
        return $off;

      case 'hm':
        $h = intval($off / 3600);
        $off = abs($off - $h * 3600);
        $m = intval($off / 60);
        return sprintf('%02d:%02d', $h, $m);
    }
  }

}

class DrushFieldsReadFormatterEntityType extends DrushFieldsReadFormatter {

  protected $format;

  /**
   * Constructor.
   *
   * @param string $format
   *   Format: type, bundle, id, label.
   * @param string $name
   *   Name.
   * @param string $desc
   *   Description.
   */
  public function __construct($format, $name = NULL, $desc = NULL) {
    if (is_null($name)) {
      $name = '';
    }
    if (is_null($desc)) {
      $desc = '';
    }
    parent::__construct($name, $desc);
    $this->format = $format;
  }

  /**
   * Formatting.
   *
   * @param int $delta
   *   Delta.
   * @param mixed $value
   *   Value.
   * @param array $general
   *   General field info.
   *
   * @return string
   *   Formatted value.
   */
  protected function format($delta, &$value, array &$general) {
    $type = $general['settings']['target_type'];
    $wrapper = entity_metadata_wrapper($type, $value);
    switch ($this->format) {
      case 'type':
        return $wrapper->type();

      case 'bundle':
        return $wrapper->getBundle();

      case 'id':
        return $wrapper->getIdentifier();

      case 'label':
        return $wrapper->label();
    }
  }

}

class DrushFieldsReadFormatterPath extends DrushFieldsReadFormatter {

  /**
   * Constructor.
   *
   * @param string $name
   *   Name.
   * @param string $desc
   *   Description.
   * @param array|NULL $sub
   *   Sub.
   */
  public function __construct($name = NULL, $desc = NULL, $sub = NULL) {
    if (is_null($name)) {
      $name = dt('Path');
    }
    if (is_null($desc)) {
      $desc = dt('Absolute filesystem path.');
    }
    parent::__construct($name, $desc, $sub);
  }

  /**
   * Formatting.
   *
   * @param int $delta
   *   Delta.
   * @param mixed $value
   *   Value.
   *
   * @return string
   *   Formatted value.
   */
  protected function format($delta, &$value) {
    return drupal_realpath($this->sub($value));
  }

}

class DrushFieldsReadFormatterUrl extends DrushFieldsReadFormatter {

  /**
   * Constructor.
   *
   * @param string $name
   *   Name.
   * @param string $desc
   *   Description.
   * @param array|NULL $sub
   *   Sub.
   */
  public function __construct($name = NULL, $desc = NULL, $sub = NULL) {
    if (is_null($name)) {
      $name = dt('URL');
    }
    if (is_null($desc)) {
      $desc = dt('Web address.');
    }
    parent::__construct($name, $desc, $sub);
  }

  /**
   * Formatting.
   *
   * @param int $delta
   *   Delta.
   * @param mixed $value
   *   Value.
   *
   * @return string
   *   Formatted value.
   */
  protected function format($delta, &$value) {
    return file_create_url($this->sub($value));
  }

}

class DrushFieldsReadFormatterLabel extends DrushFieldsReadFormatter {

  /**
   * Constructor.
   *
   * @param string $name
   *   Name.
   * @param string $desc
   *   Description.
   * @param array|NULL $sub
   *   Sub.
   */
  public function __construct($name = NULL, $desc = NULL, $sub = NULL) {
    if (is_null($name)) {
      $name = dt('Label');
    }
    if (is_null($desc)) {
      $desc = dt('List element label.');
    }
    parent::__construct($name, $desc, $sub);
  }

  /**
   * Formatting.
   *
   * @param int $delta
   *   Delta.
   * @param mixed $value
   *   Value.
   * @param array $general
   *   General field info.
   *
   * @return string
   *   Formatted value.
   */
  protected function format($delta, &$value, array &$general) {

    $l = $general['settings']['allowed_values'];

    switch ($general['type']) {
      case 'list_boolean':
      case 'list_integer':
        $v = (int) $value;
        break;

      default:
        $v = (string) $value;
    }

    return $l[$v];

  }

}

class DrushFieldsReadFormatterTaxonomyTerm extends DrushFieldsReadFormatter {

  protected $format;

  /**
   * Constructor.
   *
   * @param string $format
   *   Format: tid, name.
   * @param string $name
   *   Name.
   * @param string $desc
   *   Description.
   */
  public function __construct($format, $name = NULL, $desc = NULL) {
    if (is_null($name)) {
      $name = '';
    }
    if (is_null($desc)) {
      $desc = '';
    }
    parent::__construct($name, $desc);
    $this->format = $format;
  }

  /**
   * Formatting.
   *
   * @param int $delta
   *   Delta.
   * @param mixed $value
   *   Value.
   * @param array $general
   *   General field info.
   *
   * @return string
   *   Formatted value.
   */
  protected function format($delta, &$value, array &$general) {
    switch ($this->format) {
      case 'tid':
        return $value->tid;

      case 'name':
        return $value->name;
    }
  }

}

/**
 * Process commandline argument --template.
 *
 * If not specified provide default template.
 * If empty then print usage and * return FALSE.
 * If specified - use that.
 *
 * @param object $obj
 *   Formatters container.
 *
 * @return string|bool
 *   Return template as string.
 *   Return FALSE on error.
 */
function drush_fields_arg_template($obj) {

  $template = drush_get_option('template', NULL);
  $default = $obj->defaultTemplate();

  switch (gettype($template)) {

    case 'NULL':
      return $default;

    case 'boolean':
      drush_set_error(dt('Make sure you use single quotes from shell when setting template.'));
      drush_set_error(dt('Placeholder format is {F:K} where F=printf-style-formatter and K=Key.'));
      drush_set_error(dt('Default is: !tpl', array('!tpl' => $default)));

      drush_fields_keys($obj);

      if ($obj->universalGet()) {
        $list = array();
        $list[] = dt('~key|key|key|... - Universal key for structures.');
        $list[] = dt('~ - Value.');
        drush_fields_options($list, dt('Universal structure traversal:'));
      }

      $structurals = $obj->structuralsGet();
      if (!empty($structurals)) {
        drush_fields_options($structurals, dt('Alternatively --template may equal to generic format ID:'));
      }
      return FALSE;

    case 'string':
      return $template;

    default:
      return drush_set_error(dt('Internal error. Invalid template type.'));

  }

}

/**
 * Print out possible keys with description.
 *
 * @param object $obj
 *   Key container.
 */
function drush_fields_keys($obj) {
  $list = array();
  foreach ($obj->keysGet() as $key => $formatter) {
    $list[] = sprintf('%s - %s', $key, $formatter->description());
  }
  sort($list);
  drush_fields_options($list);
}
