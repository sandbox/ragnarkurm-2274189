<?php

/**
 * @file
 * Command implementations for File and Image fields.
 */

class DrushFieldsFieldFile extends DrushFieldsFile {}
class DrushFieldsFieldImage extends DrushFieldsFile {}

class DrushFieldsFile extends DrushFields {

  /**
   * Field-specific command description implementation.
   *
   * @return array
   *   Field-specific command description.
   */
  static public function updateCmd() {

    return array(
      'description' => array(
        'Make sure file(s) gets proper mode/owner/group or you run into trouble later. (File, Image)',
      ),
      'value' => array(
        'File, directory or URL to be added. (File, Image)',
        'If file, then extension is checked against allowed file extensions. (File, Image)',
        'If directory, then all files with allowed file extensions are added. (File, Image)',
      ),
      'options' => array(
        'mime' => 'Optional. MIME type of file object. Default = detected. (File, Image)',
        'transfer' => 'Optional. <copy|move|retrieve>:<replace|rename|error>. For URL use retrieve. Defult = copy:rename. (File, Image)',
        'uid' => 'Optional. UID of file object in Drupal. Default = entity owner. (File, Image)',
        'chown' => 'Optional. Give owner to files so that Drupal is able to manage them. (Unix. Needs elevated perms.) (File, Image)',
        'chgrp' => 'Optional. Give group to files so that Drupal is able to manage them. (Unix. Needs elevated perms.) (File, Image)',
        'chmod' => 'Optional. Give permissions to files so that Drupal is able to manage them. (Unix. Needs elevated perms.) (File, Image)',
        'description' => 'Optional. File description. Default = basename. (File)',
        'alt' => 'Optional. Set alt text for image(s). (Image)',
        'imagetitle' => 'Optional. Set title text for image(s). (Image)',
      ),
      'examples' => array(
        'drush fields-update node 1234 field_pdf --op=set --value=\'/tmp/paper.pdf\''
        => 'Set field_pdf value to paper.pdf in node 1234.',
        'drush fields-update node 1234 field_pdf --op=add --value=\'/home/user/docs\' --transfer=copy:rename --uid=1 --mime=text/plain --chmod=644 --chown=www-data --chgrp=www-data --description=\'Text file.\''
        => 'Add field a batch of files from /home/user/docs/ filtered by allowed extensions. Files will be copied and renamed if needed. In Drupal user 1 will be owner. In Drupal file description will be \'Text file.\'. In Drupal MIME type will be text/plain. In filesystem, files will have owner:group:perm as www-data:www-data:644.',
        'drush fields-update node 1234 field_photo --op=set --value=\'/tmp/DSC0001.jpg\''
        => 'Set field_photo to DSC0001.jpg in node 1234.',
        'drush fields-update node 1234 field_gallery --op=add --value=\'/tmp/pics\' --transfer=copy:rename --uid=1 --mime=image/jpeg --chmod=644 --chown=www-data --chgrp=www-data --alt=\'img\' --imagetitle=\'An image.\''
        => 'Add into field_gallery a batch of files from /tmp/pics filtered by allowed extensions. Files will be copied and renamed if needed. In Drupal, user 1 will be owner. In Drupal image alternate text will be \'img\' and title will be \'An image.\'. In Drupal MIME type will be image/jpeg. In filesystem, files will have owner:group:perm as www-data:www-data:644.',
      ),
    );

  }

  /**
   * Field-specific implementation of update preparation.
   *
   * @param string $values
   *   Command-line argument --value value.
   *
   * @return array
   *   Field-specific values to set/add.
   */
  protected function updatePrepare($values) {

    $extensions = $this->instance['settings']['file_extensions'];
    $values = drush_fields_files_parse($values, $extensions);
    if ($values === FALSE) {
      return FALSE;
    }

    // Though this is checked also later, but we do before messing with files.
    if (!$this->cardinalityCheck(count($values), 'add')) {
      return FALSE;
    }
    $uid = drush_fields_arg_uid($this->entity);
    if ($uid === FALSE) {
      return FALSE;
    }
    $chmod = drush_fields_arg_chmod();
    if ($chmod === FALSE) {
      return FALSE;
    }
    $chown = drush_fields_arg_chown();
    if ($chown === FALSE) {
      return FALSE;
    }
    $chgrp = drush_fields_arg_chgrp();
    if ($chgrp === FALSE) {
      return FALSE;
    }
    $transfer = drush_fields_arg_transfer($values);
    if ($transfer === FALSE) {
      return FALSE;
    }

    $destinations = drush_fields_file_destination($values, $this->instance);

    $list = array();

    foreach ($destinations as $f => $uri) {
      $mime = drush_fields_arg_mime($uri);
      if ($mime === FALSE) {
        continue;
      }

      $file = (object) array(
        'uid' => $uid,
        'filemime' => $mime,
        'uri' => $f,
        'status' => 1,
        'display' => 1,
      );

      switch ($this->fieldType) {
        case 'file':
          $file->description = (string) drush_get_option('description', drupal_basename($f));
          break;

        case 'image':
          $file->alt = (string) drush_get_option('alt', '');
          $file->title = (string) drush_get_option('imagetitle', '');
          break;
      }

      $file = drush_fields_file_takeover($f, $file, $uri, $transfer['method'], $transfer['resolution'], $chmod, $chown, $chgrp);
      if (empty($file)) {
        // Error has been reported, but continue.
        // Cannot stop in the middle of action.
        continue;
      }

      $file = (array) $file;
      $list[] = $file;
    }

    return $list;

  }

  /**
   * Field-specific 'read' template.
   *
   * @return string
   *   Return template string.
   */
  public function defaultTemplate() {
    switch ($this->fieldType) {
      case 'file':
        return '{%6d:delta} {%6d:fid} {%6d:uid} {%-20s:mime} {%17s:time} {%9d:size} {%s:filename}';

      case 'image':
        return '{%6d:delta} {%6d:fid} {%6d:uid} {%6s:width} {%6s:height} {%17s:time} {%9d:size} {%s:filename}';
    }
  }

  /**
   * Field-specific 'read' keys.
   *
   * @return array
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {

    $keys = array(
      'delta' => new DrushFieldsReadFormatterDelta(),
      'fid' => new DrushFieldsReadFormatterValue(dt('FID'), dt('File object ID in Drupal database.'), array('fid')),
      'uid' => new DrushFieldsReadFormatterValue(dt('UID'), dt('File owner object ID in Drupal database.'), array('uid')),
      'mime' => new DrushFieldsReadFormatterValue(dt('MIME'), dt('File MIME type.'), array('filemime')),
      'time' => new DrushFieldsReadFormatterDateTime('Y-M-d H:i', dt('Created'), dt('File creation time, in human format.'), array('timestamp')),
      'epoch' => new DrushFieldsReadFormatterDateTime('U', dt('Created'), dt('File creation time, in epoch format.'), array('timestamp')),
      'size' => new DrushFieldsReadFormatterValue(dt('Size'), dt('File size, in bytes.'), array('filesize')),
      'filename' => new DrushFieldsReadFormatterValue(dt('Filename'), dt('File name.'), array('filename')),
      'file-url' => new DrushFieldsReadFormatterUrl(NULL, NULL, array('uri')),
      'file-absolute' => new DrushFieldsReadFormatterPath(NULL, NULL, array('uri')),
      'uri' => new DrushFieldsReadFormatterValue(dt('URI'), dt('Drupal stream wrapper.'), array('uri')),
      'status' => new DrushFieldsReadFormatterValue(dt('Status'), dt('???'), array('status')),
      'display' => new DrushFieldsReadFormatterValue(dt('Display'), dt('???'), array('display')),
    );

    switch ($this->fieldType) {
      case 'file':
        $keys['description'] = new DrushFieldsReadFormatterValue(dt('Description'), dt('User-emtered description of a file.'), array('description'));

      case 'image':
        $keys['width'] = new DrushFieldsReadFormatterValue(dt('Width'), dt('Image width.'), array('width'));
        $keys['height'] = new DrushFieldsReadFormatterValue(dt('Height'), dt('Image height.'), array('height'));
        $keys['alt'] = new DrushFieldsReadFormatterValue(dt('Alt'), dt('Alternate text.'), array('alt'));
        $keys['title'] = new DrushFieldsReadFormatterValue(dt('Title'), dt('Title text.'), array('title'));

    }

    return $keys;

  }

}

/**
 * Check if a file, directory or url is eligible for "upload" to drupal.
 *
 * Check if file is accessible, if is a regular file or directory, if is
 * readable. If file or directory is not eligible then issue an error and
 * return FALSE.
 *
 * @param string $file
 *   Value specified from commandline.
 * @param array $extensions
 *   Allowed file extensions from field_instance -> settings -> file_extensions.
 *
 * @return array|bool
 *   List of files or FALSE on error.
 */
function drush_fields_files_parse($file, array $extensions) {

  $is_url = preg_match('!^[a-zA-Z]+://!', $file);
  if (!$is_url) {
    if (!file_exists($file)) {
      return drush_set_error(dt('!f: File not found / Permission denied / Invalid path.', array('!f' => $file)));
    }
    if (!is_readable($file)) {
      return drush_set_error(dt('!f: Must be readable.', array('!f' => $file)));
    }
  }

  $rx = preg_replace('/^[, ]+|[, ]+$/', '', $extensions);
  $rx = preg_replace('/[, ]+/', '|', $rx);
  $rx = preg_replace('/([^a-zA-Z0-9|])/', '\\$1', $rx);
  $rx = "/\.($rx)\$/i";
  if ($is_url || is_file($file)) {
    if (!preg_match($rx, $file)) {
      drush_print(dt('!f: Warning: File does not match allowed extensions: !ext.', array('!f' => $file, '!ext' => $extensions)));
    }
    return array($file);
  }
  if (is_dir($file)) {
    $file = preg_replace('!/+$!', '', $file);
    $files = file_scan_directory($file, $rx);
    $files = array_keys($files);
    return $files;
  }

  return drush_set_error(dt('!f: Must be regular file or directory.', array('!f' => $file)));

}

/**
 * Chew commandline argument --chown.
 *
 * If specified, then validate and convert to uid if necessary.
 *
 * @return int|NULL|bool
 *   If commandline is valid then return uid.
 *   If not specified return NULL.
 *   If error, return FALSE.
 */
function drush_fields_arg_chown() {

  $owner = drush_get_option('chown', NULL);

  if (is_null($owner)) {
    return NULL;
  }

  if (preg_match('/^\d+$/', $owner)) {
    return (int) $owner;
  }

  if (preg_match('/^[a-z][a-z0-9._-]+$/', $owner)) {
    $user = posix_getpwnam($owner);
    if (empty($user)) {
      return drush_set_error(dt('--chown=!owner: Invalid owner. Unable to get uid.', array('!owner' => $owner)));
    }
    return (int) $user['uid'];
  }

  return drush_set_error(dt('--chown=!owner: Invalid owner. Expected system uid or username.', array('!owner' => $owner)));

}

/**
 * Chew commandline argument --chgrp.
 *
 * If specified, then validate and convert to gid if necessary.
 *
 * @return int|NULL|bool
 *   If commandline is valid then return gid.
 *   If not specified return NULL.
 *   If error, return FALSE.
 */
function drush_fields_arg_chgrp() {

  $group = drush_get_option('chgrp', NULL);

  if (is_null($group)) {
    return NULL;
  }

  if (preg_match('/^\d+$/', $group)) {
    return (int) $group;
  }

  if (preg_match('/^[a-z][a-z0-9._-]+$/', $group)) {
    $grp = posix_getgrnam($group);
    if (empty($grp)) {
      return drush_set_error(dt('--chgrp=!group: Invalid group. Unable to get gid.', array('!group' => $group)));
    }
    return (int) $grp['gid'];
  }

  return drush_set_error(dt('--chgrp=!group: Invalid group. Expected system gid or groupname.', array('!group' => $group)));

}

/**
 * Chew commandline argument --chmod.
 *
 * If specified, then validate and convert to decimal if necessary.
 *
 * @return int|NULL|bool
 *   If commandline is valid then return mode.
 *   If not specified return NULL.
 *   If error, return FALSE.
 */
function drush_fields_arg_chmod() {

  $mode = drush_get_option('chmod', NULL);

  if (is_null($mode)) {
    return NULL;
  }

  if (!preg_match('/^[0-7]{3,4}$/', $mode)) {
    return drush_set_error(dt('--chmod=!mode: Invalid mode. Expected 3-4 digits octal number.', array('!mode' => $mode)));
  }

  $mode = octdec($mode);
  return $mode;

}

/**
 * Copy or move file to Drupal realm and set owner, group and permissions.
 *
 * Tested on Linux. Developer needs feedback on Windows, specifically how does
 * drupal_chmod(), chown() and chgrp() behave and if/how it can/need be tested.
 *
 * @param string $filename
 *   Filename with path.
 * @param array $file
 *   File object "prototype".
 * @param string $uri
 *   File URI.
 * @param string $method
 *   Function name: "file_move", "file_copy" or "system_retrieve_file".
 * @param string $resolution
 *   How to handle conflicts? See file_move() or file_copy() doc.
 * @param int|NULL $chmod
 *   Set file permissions.
 * @param int|NULL $chown
 *   Set file owner.
 * @param int|NULL $chgrp
 *   Set file group.
 *
 * @return object|NUL
 *   Return file object or NULL if $method failed.
 */
function drush_fields_file_takeover($filename, array $file, $uri, $method, $resolution, $chmod, $chown, $chgrp) {

  switch ($method) {
    case 'file_copy':
    case 'file_move':
      $file = $method($file, $uri, $resolution);
      break;

    case 'system_retrieve_file':
      /* system_retrieve_file() creates its own file object. So we need to copy
      some essentials to it. unset some fields from template. The apply it to
      new file object. */
      $f = (array) $file;
      unset($f['fid']);
      unset($f['uri']);
      unset($f['filename']);
      unset($f['timestamp']);
      unset($f['filesize']);
      $file = $method($filename, $uri, TRUE, $resolution);
      if (!empty($file)) {
        $file = (object) array_merge((array) $file, (array) $f);
      }
      break;

    default:
      return drush_set_error(dt('!f: Internal error. Debug: method=!method', array('!f' => __FUNCTION__, '!method' => $method)));
  }
  if (empty($file)) {
    drush_set_error(dt('!f: Warning: Failed creating file object in database.', array('!f' => $filename)));
    // Don't exit on error. User may lose moved file.
    return NULL;
  }

  if (is_int($chmod)) {
    if (!drupal_chmod($file->uri, $chmod)) {
      drush_set_error(dt('!f: Warning: Failed chmod.', array('!f' => $filename)));
      // Don't exit on error. User may lose moved file.
    }
  }

  if (is_int($chown)) {
    $absolute = drupal_realpath($file->uri);
    if (!chown($absolute, $chown)) {
      drush_set_error(dt('!f: Warning: Failed chown.', array('!f' => $filename)));
      // Don't exit on error. User may lose moved file.
    }
  }

  if (is_int($chgrp)) {
    $absolute = drupal_realpath($file->uri);
    if (!chgrp($absolute, $chgrp)) {
      drush_set_error(dt('!f: Warning: Failed chgrp.', array('!f' => $filename)));
      // Don't exit on error. User may lose moved file.
    }
  }

  return $file;

}

/**
 * Build destination path for "upload".
 *
 * @param array $files
 *   Absolute file paths of source files to be added.
 * @param array $field_instance
 *   Ouptut of field_info_instance().
 *
 * @return array
 *   File path => URI for file "upload".
 */
function drush_fields_file_destination(array $files, array $field_instance) {

  $destination = $field_instance['settings']['file_directory'];
  $destinations = array();
  foreach ($files as $f) {
    $file = drupal_basename($f);
    $dest = "$destination/$file";
    $dest = file_build_uri($dest);
    $destinations[$f] = $dest;
  }
  return $destinations;

}

/**
 * Parse --transfer option.
 *
 * @param array $files
 *   Files to be transferred. For checking if local (move, copy) or remote
 *   (retrieve).
 *
 * @return array|bool
 *   Array containing method function (file_copy, file_move,
 *   system_retrieve_file) and conflict resolution constant
 *   (FILE_EXISTS_REPLACE, FILE_EXISTS_RENAME, FILE_EXISTS_ERROR).
 *   Return FALSE on error.
 */
function drush_fields_arg_transfer(array &$files) {

  $is_url = preg_match('!^[a-zA-Z]+://!', $files[0]);

  $transfer = drush_get_option('transfer', NULL);

  if (empty($transfer)) {
    return array(
      'method' => $is_url ? 'system_retrieve_file' : 'file_copy',
      'resolution' => FILE_EXISTS_RENAME,
    );
  }

  $pieces = explode(':', $transfer);
  if (count($pieces) != 2) {
    return drush_set_error(dt('--transfer=!t: Expected 2 colon-separated values.', array('!t' => $transfer)));
  }
  list($method, $resolution) = $pieces;

  switch ($method) {
    case 'copy':
      $method = 'file_copy';
      break;

    case 'move':
      $method = 'file_move';
      break;

    case 'retrieve':
      $method = 'system_retrieve_file';
      break;

    default:
      return drush_set_error(dt('--transfer=!t: Method (!m) must be "copy", "move" or "retrieve".', array('!t' => $transfer, '!m' => $method)));
  }

  switch ($resolution) {
    case 'replace':
      $resolution = FILE_EXISTS_REPLACE;
      break;

    case 'rename':
      $resolution = FILE_EXISTS_RENAME;
      break;

    case 'error':
      $resolution = FILE_EXISTS_ERROR;
      break;

    default:
      return drush_set_error(dt('--transfer=!t: Resolution (!r) must be "replace", "rename" or "error".', array('!t' => $transfer, '!r' => $resolution)));
  }

  return array(
    'method' => $method,
    'resolution' => $resolution,
  );

}

/**
 * Determine MIME type.
 *
 * Determine MIME type explicitly by commandline option or if not specified use
 * common heuristics.
 *
 * @param string $file
 *   File name, file with path or URI for which MIME type should be determined.
 *
 * @return string|false
 *   MIME type or FALSE on error.
 */
function drush_fields_arg_mime($file) {

  $mime = drush_get_option('mime', NULL);

  if (!empty($mime)) {
    $mime = strtolower($mime);
    $rx = '^[a-z0-9.-]+/[a-z0-9.-]+$';
    if (!preg_match("!$rx!", $mime)) {
      return drush_set_error(dt('--mime=!m: Invalid MIME type format. Must comply to regex: !rx.', array('!m' => $mime, '!rx' => $rx)));
    }
    return $mime;
  }

  $mime = file_get_mimetype($file);
  if (empty($mime)) {
    return drush_set_error(dt('!f: Unable to detect mime type.', array('!f' => $file)));
  }
  return $mime;

}

/**
 * Determine file owner uid.
 *
 * Based on Entity owner or commandline option --uid determine who will own
 * file object.
 *
 * @param object $entity
 *   Usually a node object.
 *
 * @return int|bool
 *   UID, or FALSE on error
 */
function drush_fields_arg_uid($entity) {

  $uid = drush_get_option('uid', NULL);

  if (empty($uid) && $uid !== '0') {
    if (property_exists($entity, 'uid')) {
      return (int) ($entity->uid);
    }
    else {
      return drush_set_error(dt('--uid: Not specified and entity does not provide one.'));
    }
  }
  else {
    if (!is_numeric($uid)) {
      return drush_set_error(dt('--uid=!u: Must be numeric.', array('!u' => $uid)));
    }
    $user = user_load($uid);
    if (empty($user)) {
      return drush_set_error(dt('--uid=!u: Must be existing user / Cannot load user object.', array('!u' => $uid)));
    }
    return (int) $uid;
  }

}
