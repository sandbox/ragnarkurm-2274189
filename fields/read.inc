<?php

/**
 * @file
 * A Field data read functionality.
 */

/**
 * Provide Drush commands this "plugin" can handle.
 *
 * @return array
 *   Return an array of Drush commands.
 */
function drush_fields_read_cmd() {

  return array(

    'fields-read' => array(
      'description' => 'Read data elements in a Field.',
      'callback' => 'drush_fields_read',
      'arguments' => array(
        'entity type machine name' => 'Required. Example: node',
        'entity id' => 'Required. Example: 1234',
        'field machine name' => 'Required. Example: field_message',
      ),
      'options' => array(
        'template' => 'Optional. Custom output template.'
        . "\nLeave empty to get list of options.",
        'header' => 'Optional. Display header or not? ' . drush_fields_yn_desc('y'),
      ),
      'examples' => array(
        'drush fields-read node 1234 field_message'
        => 'Read all elements in field_message of node 1234.',
        'drush fields-read node 1234 field_message --template=\'{%10d:delta} {%s:~summary}\' --header=n'
        => 'Read all elements in field_message of node 1234 with given template and without header.',
      ),
      'required-arguments' => 3,
      /* Have to be able to use --user option. */
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
      'core' => array('7'),
      'drupal dependencies' => array(
        'entity',
      ),
    ),

  );

}

/**
 * Implements field element read.
 *
 * @param string $entity_type
 *   Entity machine name. Examples: node, profile2.
 * @param string|int $entity_id
 *   Entity ID (nid, uid, etc). Examples: 1, 10, 100, 31415.
 * @param string $field_name
 *   Field machine name. Example: field_count.
 */
function drush_fields_read($entity_type = NULL, $entity_id = NULL, $field_name = NULL) {

  drush_fields_require_once_classes();

  $field = new DrushFields($entity_type, $entity_id, $field_name);
  if ($field->errorGet() === TRUE) {
    return FALSE;
  }

  // Universal / Field-specific approach.
  $f = $field->extend();
  if (!is_null($f)) {
    $field = $f;
  }

  $template = drush_fields_arg_template($field);
  if ($template === FALSE) {
    return FALSE;
  }

  $header = drush_fields_yn(drush_get_option('header', TRUE), TRUE);
  if (is_null($header)) {
    return FALSE;
  }

  return $field->read($template, $header);

}
