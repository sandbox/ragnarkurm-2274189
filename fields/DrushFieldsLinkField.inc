<?php

/**
 * @file
 * Command implementations for Link field.
 */

class DrushFieldsFieldLinkField extends DrushFields {

  /**
   * Field-specific command description implementation.
   *
   * @return array
   *   Field-specific command description.
   */
  static public function updateCmd() {

    return array(
      'value' => array(
        'URL. (Link)',
      ),
      'options' => array(
        'urltitle' => 'Title of link. (Link)',
      ),
      'examples' => array(
        'drush fields-update node 1234 field_email --op=set --value=\'john.doe@example.com\''
        => 'Set field_email to \'john.doe@example.com\' of node 1234.',
      ),
    );

  }

  /**
   * Field-specific implementation of update preparation.
   *
   * @param string $values
   *   Command-line argument --value value.
   *
   * @return array
   *   Field-specific values to set/add.
   */
  protected function updatePrepare($values) {

    $title = drush_get_option('urltitle', NULL);
    $title = (empty($title) || !is_string($title)) ? '' : $title;

    $values = array(
      'url' => $values,
      'title' => $title,
    );
    $values = array($values);

    return $values;

  }

  /**
   * Field-specific 'read' template.
   *
   * @return string
   *   Return template string.
   */
  public function defaultTemplate() {
    return '{%6d:delta} {%s:url} {%s:title}';
  }

  /**
   * Field-specific 'read' keys.
   *
   * @return array
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {

    return array(
      'delta' => new DrushFieldsReadFormatterDelta(),
      'url' => new DrushFieldsReadFormatterValue(dt('URL'), dt('Uniform Resource Locator.'), array('url')),
      'title' => new DrushFieldsReadFormatterValue(dt('Title'), dt('URL title.'), array('title')),
    );

  }

}
