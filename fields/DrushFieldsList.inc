<?php

/**
 * @file
 * Command implementations for List* fields.
 */

class DrushFieldsFieldListBoolean extends DrushFieldsList {}
class DrushFieldsFieldListFloat extends DrushFieldsList {}
class DrushFieldsFieldListInteger extends DrushFieldsList {}
class DrushFieldsFieldListText extends DrushFieldsList {}

class DrushFieldsList extends DrushFields {

  /**
   * Field-specific command description implementation.
   *
   * @return array
   *   Field-specific command description.
   */
  static public function updateCmd() {

    return array(
      'value' => array(
        'Keys, comma-separated. (List Text, List Integer, List Float)',
        'Boolean. ' . drush_fields_yn_desc() . ' (List Boolean)',
      ),
      'examples' => array(
        'drush fields-update node 1234 field_status --op=set --value=\'1\''
        => 'Set field_status to true of node 1234.',
        'drush fields-update node 1234 field_const --op=set --value=\'3.14\''
        => 'Set field_const to 3.14 of node 1234.',
        'drush fields-update node 1234 field_coins --op=set --value=\'1,10,50\''
        => 'Set field_coins to 1, 10 and 50 of node 1234.',
        'drush fields-update node 1234 field_options --op=set --value=\'yes,no,possibly_maybe\''
        => 'Set field_options to yes, no and possibly_maybe of node 1234.',
      ),
    );

  }

  /**
   * Field-specific implementation of update preparation.
   *
   * @param string $values
   *   Command-line argument --value value.
   *
   * @return array
   *   Field-specific values to set/add.
   */
  protected function updatePrepare($values) {

    if ($this->fieldType === 'list_boolean' && is_null(drush_get_option('set', NULL))) {
      return $this->errorSet(dt('Only --op=set is allowed for list_boolean field type.'));
    }

    $values = preg_replace('/\s+/', '', $values);
    $values = explode(',', $values);

    // Init duplication check.
    if (drush_get_option('add', NULL) === TRUE) {
      drush_fields_dups($this->values);
    }
    if (drush_get_option('set', NULL) === TRUE) {
      drush_fields_dups(array());
    }

    $ret = array();

    $l = $this->general['settings']['allowed_values'];
    foreach ($values as $v) {

      switch ($this->fieldType) {
        case 'list_boolean':
          $v = drush_fields_yn($v, NULL);
          if (is_null($v)) {
            return FALSE;
          }
          $v = (int) $v;
          break;

        case 'list_float':
          if (!is_numeric($v)) {
            return $this->errorSet(dt('!v: Invalid float.', array('!v' => $v)));
          }
          $v = (string) $v;
          break;

        case 'list_integer':
          if (!preg_match('/^-?\d+$/', $v)) {
            return $this->errorSet(dt('!v: Invalid integer.', array('!v' => $v)));
          }
          $v = (int) $v;
          break;

        case 'list_text':
          // Nothing to check.
          break;
      }

      if (!array_key_exists($v, $l)) {
        $this->errorSet(dt('--value: Invalid key \'!v\'.', array('!v' => $v)));
        $this->errorSetOptions(array_keys($l));
        return FALSE;
      }

      // Skip dups.
      if (drush_fields_dups($v) === FALSE) {
        continue;
      }

      $ret[] = $v;
    }

    return $ret;

  }

  /**
   * Field-specific 'read' template.
   *
   * @return string
   *   Return template string.
   */
  public function defaultTemplate() {
    switch ($this->fieldType) {
      case 'list_boolean':
        return '{%6d:bool} {%s:label}';

      case 'list_float':
        return '{%6d:delta} {%9s:float} {%s:label}';

      case 'list_integer':
        return '{%6d:delta} {%9d:int} {%s:label}';

      case 'list_text':
        return '{%6d:delta} {%12s:key} {%s:label}';
    }
  }

  /**
   * Field-specific 'read' keys.
   *
   * @return array
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {

    $keys = array(
      'delta' => new DrushFieldsReadFormatterDelta(),
      'label' => new DrushFieldsReadFormatterLabel(NULL, NULL),
    );

    switch ($this->fieldType) {
      case 'list_boolean':
        $keys['bool'] = new DrushFieldsReadFormatterValue(dt('Bool'), dt('Boolean value.'));
        break;

      case 'list_float':
        $keys['float'] = new DrushFieldsReadFormatterValue(dt('Float'), dt('Floating-point value.'));
        break;

      case 'list_integer':
        $keys['int'] = new DrushFieldsReadFormatterValue(dt('Int'), dt('Integer value.'));
        break;

      case 'list_text':
        $keys['text'] = new DrushFieldsReadFormatterValue(dt('Text'), dt('Text value.'));
        break;

    }

    return $keys;

  }

}
