<?php

/**
 * @file
 * Base class for field type implementation.
 */

class DrushFields {

  protected $entityType;
  protected $bundle;
  protected $id;
  protected $entity;
  protected $wrapper;

  protected $fieldType;
  protected $general;
  protected $max;

  protected $name;
  protected $instance;
  protected $min;

  protected $values;
  protected $count;

  protected $delta;

  protected $error = FALSE;

  private $structuralFormats = array(
    'print_r' => 'readPrintR',
    'serial' => 'readSerial',
    'var_export' => 'readVarExport',
    'var_dump' => 'readVarDump',
    'json' => 'readJson',
    'xml' => 'readXml',
  );

  /**
   * Load in target field. Gather all necessary related info. Perform checks.
   *
   * After object construction errorGet() method should be checked if takoff was
   * smooth.
   *
   * @param string $entity_type
   *   Entity machine name. Examples: node, profile2.
   *   Special case: if $entity_type is NULL, then just return to create object
   *   of desired type. This is needed for extend().
   * @param string|int $id
   *   Entity ID (nid, uid, etc). Examples: 1, 10, 100, 31415.
   * @param string $name
   *   Field machine name. Example: field_pdf.
   * @param string|int $delta
   *   Delta of the multivalue field element, may be NULL.
   */
  public function __construct(
    $entity_type,
    $id,
    $name,
    $delta = NULL
  ) {

    /* Allow extend(). */

    if (is_null($entity_type)) {
      return;
    }

    /* Entity type */

    $entities = entity_get_info();
    if (!array_key_exists($entity_type, $entities)) {
      $this->errorSet(dt('Entity type (!e): No such entity type.', array('!e' => $entity_type)));
      $list = array_keys($entities);
      sort($list);
      $this->errorSetOptions($list);
      return;
    }
    $this->entityType = $entity_type;

    /* Entity ID, Entity */

    if (!is_numeric($id)) {
      $this->errorSet(dt('Entity ID (!id): Must be numeric.', array('!id' => $id)));
      return;
    }
    $entity = entity_load_single($this->entityType, $id);
    if (empty($entity)) {
      $this->errorSet(dt('Entity Type (!type) and Entity ID (!id): No such combination.', array('!type' => $this->entityType, '!id' => $id)));
      return;
    }
    $this->id = $id;
    $this->entity = $entity;

    /* Wrapper */

    $wrapper = entity_metadata_wrapper($this->entityType, $this->entity);
    $this->wrapper = $wrapper;

    /* Bundle */

    $bundle = $wrapper->getBundle();
    if (empty($bundle)) {
      $this->errorSet(dt('Bundle: Unable to determine bundle.'));
      return;
    }
    $this->bundle = $bundle;

    /* Field Name, Field Type, General, Max */

    $fields = field_info_fields();
    $general = $fields[$name];
    if (empty($general)) {
      $this->errorSet(dt('Field (!f): No such field exists.', array('!f' => $name)));
      $list = array_keys($fields);
      sort($list);
      $this->errorSetOptions($list);
      return;
    }
    $this->name = $name;
    $this->general = $general;
    $this->fieldType = $this->general['type'];
    $max = (int) $this->general['cardinality'];
    $this->max = $max < 1 ? NULL : $max;

    /* Active, Deleted */

    if (!$this->general['active']) {
      $this->errorSet(dt('Field (!f): Field has to be active.', array('!f' => $name)));
      return;
    }

    if ($this->general['deleted']) {
      $this->errorSet(dt('Field (!f): Field must be non-deleted.', array('!f' => $name)));
      return;
    }

    /* Field used in entity type */

    $fbundles = $this->general['bundles'];
    if (!array_key_exists($this->entityType, $fbundles)) {
      $this->errorSet(dt('Entity type (!type) and Field (!f): Field must be used in the entity type.', array('!f' => $this->name, '!type' => $this->entityType)));
      return;
    }

    /* Field used in bundle */

    $bfields = $fbundles[$this->entityType];
    if (!in_array($this->bundle, $bfields)) {
      $this->errorSet(dt('Field (!f) and bundle (!b): Field must be used in bundle.', array('!f' => $this->name, '!b' => $this->bundle)));
      return;
    }

    /* Field instance, Min */

    $instance = field_info_instance($this->entityType, $this->name, $this->bundle);
    if (empty($instance)) {
      $this->errorSet(dt(
        'Entity type (!type), bundle (!bundle) and field (!field): Unable to fetch instance info.',
        array(
          '!type' => $this->entityType,
          '!name' => $this->name,
          '!bundle' => $this->bundle,
        )
      ));
      return;
    }
    $this->instance = $instance;
    $min = $this->instance['required'] ? 1 : 0;
    $this->min = $min;

    /* Value, Count */

    $values = $wrapper->{$this->name}->value();
    if ($this->max === 1) {
      $values = empty($values) ? array() : array($values);
    }
    $this->values = $values;
    $this->count = count($this->values);

    /* Delta */

    $type = gettype($delta);
    switch ($type) {
      case 'NULL':
        break;

      case 'string':
        if (!preg_match('/^\d+$/', $delta)) {
          $this->errorSet(dt('Delta (!delta): Must be non-negative integer.', array('!delta' => $delta)));
          return;
        }
        $delta = (int) $delta;
      case 'integer':
        if (!$this->count) {
          $this->errorSet(dt('Delta (!d): This field is empty.', array('!d' => $delta)));
          return;
        }
        if ($this->count - 1 < $delta) {
          $this->errorSet(dt('Delta (!d): Delta must be less than number of values (!count) the field holds..', array('!d' => $delta, '!count' => $this->count)));
          return;
        }
        if (!array_key_exists($delta, $this->values)) {
          $this->errorSet(dt('Delta (!d): Value must exist in this delta position.', array('!d' => $delta)));
          return;
        }
        break;

      default:
        $this->errorSet(dt('Delta: Must be non-negative integer.'));
        return;
    }
    $this->delta = $delta;

  }

  /**
   * Extend general DrushFields class to field-specific DrushFields_type.
   *
   * Creates new field-specific object out of general one.
   *
   * @return object|NULL
   *   Field-specific object.
   *   FALSE - There is no field-specific class.
   */
  public function extend() {
    $class = $this->fieldType;
    $class = strtr($class, '_', ' ');
    $class = ucwords($class);
    $class = strtr($class, array(' ' => ''));
    $class = "DrushFieldsField$class";
    if (!class_exists($class)) {
      return NULL;
    }

    $new = new $class(NULL, NULL, NULL);
    foreach (get_object_vars($this) as $k => $v) {
      $new->{$k} = $v;
    }

    return $new;
  }

  /**
   * Print error and set error status.
   *
   * @param string $error
   *   Translated message.
   *
   * @return bool
   *   FALSE.
   */
  protected function errorSet($error) {
    $this->error = TRUE;
    return drush_set_error($error);
  }

  /**
   * Print list of options to user.
   *
   * @param array $list
   *   List of options as array values.
   *
   * @return bool
   *   FALSE.
   */
  protected function errorSetOptions(array $list) {
    $this->error = TRUE;
    return drush_fields_options($list);
  }

  /**
   * Check status of last operation.
   *
   * This is to check if constructor failed.
   *
   * @return bool
   *   Return TRUE if there were errors.
   *   Return FALSE if it went smoothly.
   */
  public function errorGet() {
    return $this->error;
  }

  /**
   * Check user access to the field.
   *
   * Check is skipped for UID 0 and 1.
   * First checks entity level permissions.
   * Secondly checks field level permissions.
   *
   * @param string $op
   *   One of 'view', 'update', 'create' or 'delete'.
   * @param bool $error
   *   TRUE - Print access errors.
   *   FALSE - keep quiet, only return access info.
   *
   * @return bool
   *   TRUE if uid is 0 or 1.
   *   TRUE if access granted (entity and field level).
   *   FALSE if access denied.
   */
  public function access($op, $error = FALSE) {

    $this->error = FALSE;

    static $map = array(
      'view' => 'view',
      'update' => 'edit',
      'create' => 'edit',
      'delete' => 'edit',
    );
    $fop = $map[$op];
    if (empty($fop)) {
      return $this->errorSet(dt('!f: Internal error. Operation = \'!op\'.', array('!f' => __FUNCTION__, '!op' => $op)));
    }

    /* User check */

    $uid = (int) $GLOBALS['user']->uid;
    if ($uid === 0 || $uid === 1) {
      return TRUE;
    }

    /* Entity check */

    if (!entity_access($op, $this->entityType, $this->entity)) {
      if ($error) {
        return $this->errorSet(dt('You dont have \'!op\' permission to the entity.', array('!op' => $op)));
      }
      else {
        return FALSE;
      }
    }

    /* Field Check */

    if (!field_access($fop, $this->general, $this->entityType, $this->entity)) {
      if ($error) {
        return $this->errorSet(dt('You dont have \'!op\' permission to the field.', array('!op' => $fop)));
      }
      else {
        return FALSE;
      }
    }

    /* Entity and Field checks were successful */

    return TRUE;
  }

  /**
   * Check if number of field element(s) can be changed.
   *
   * Check min/max and determine if change is possible.
   * If not, then print error.
   *
   * @param int $amount
   *   Amount according to $op.
   * @param string $op
   *   One of: "add", "del", "set".
   *
   * @return bool
   *   TRUE - Operation is possible.
   *   FALSE - Operation is not possible.
   */
  protected function cardinalityCheck($amount, $op) {

    $this->error = FALSE;

    switch ($op) {
      case 'add':
        $new = $this->count + $amount;
        break;

      case 'del':
        $new = $this->count - $amount;
        break;

      case 'set':
        $new = $amount;
        break;

      default:
        return $this->errorSet(dt('!f: Internal error. Operation = !op', array('!f' => __FUNCTION__, '!op' => $op)));
    }

    if ($this->min > $new) {
      return $this->errorSet(dt('Requested operation would result in less elements (!new) than the field requires (!min).', array('!min' => $this->min, '!new' => $new)));
    }

    if (is_int($this->max) && $this->max < $new) {
      return $this->errorSet(dt('Requested operation would result in more elements (!new) than the field allows (!max).', array('!max' => $this->max, '!new' => $new)));
    }

    return TRUE;
  }

  /**
   * Save changes to entity. Mostly, take care of cardinality=1 case.
   *
   * This module keeps value always as array, but a Drupal field keeps value as
   * element if cardinality=1 and otherwise as an array. This makes all code
   * complicated, so we keep alwas it as an array.
   *
   * @param string $method
   *   How to save an entity? Possible options are:
   *   - entity_save.
   *   - field_attach_update.
   */
  public function save($method) {

    $this->error = FALSE;

    /* Build up $values to save. */

    /* If no elements, then we have to use NULL. */
    if ($this->count === 0) {
      $values = NULL;
    }

    /* If cardinality is set to 1
     * then values must be equal to element. */
    elseif ($this->max === 1) {
      $values = $this->values[0];
    }

    /* Otherwise values must contain an array. */
    else {
      $values = $this->values;
    }

    /* Set new value */
    $this->wrapper->{$this->name}->set($values);

    /* Validate field data. */
    /* Inspired by modules/field_ui/field_ui.admin.inc */

    $lang = $this->wrapper->language->value();
    $values = $this->entity->{$this->name}[$lang];

    $errors = NULL;
    $function = $this->general['module'] . '_field_validate';
    if (function_exists($function)) {

      $function(
        $this->entityType,
        $this->entity,
        $this->general,
        $this->instance,
        $lang,
        $values,
        $errors
      );

      $e = &$errors[$this->name];
      if (!empty($e)) {
        $e = $e[$lang];
        if (!empty($e)) {
          foreach ($e as $delta => $msgs) {
            foreach ($msgs as $msg) {
              $m = $msg['message'];
              $m = preg_replace('/<em[^>]*>|<\/em>/', '**', $m);
              $this->errorSet(dt('Invalid value. Delta: !delta. Error: !error', array('!delta' => $delta, '!error' => $m)));
            }
          }
        }
        return FALSE;
      }

    }

    /* Confirmation. */

    if (!drush_confirm(dt('Save field data?'))) {
      return drush_user_abort();
    }

    /* Actual saving. */

    switch ($method) {
      case 'entity_save':
        $this->wrapper->save();
        return;

      case 'field_attach_update':
        field_attach_update($this->entityType, $this->entity);
        entity_get_controller($this->entityType)->resetCache(array($this->id));
        return;

      default:
        return drush_set_error(dt('!f: Internal error. Method = !method', array(
          '!f' => __FUNCTION__,
          '!method' => $method,
        )));
    }

  }

  /**
   * Return number of elements in a field.
   *
   * @return int
   *   Return number of elements in a field.
   */
  public function count() {

    $this->error = FALSE;

    // Print errors.
    if ($this->access('view', TRUE) == FALSE) {
      return FALSE;
    }

    drush_print($this->count);

  }

  /**
   * Delete element(s) from field according to constructor delta.
   *
   * @param string $save_method
   *   Save menthod.
   */
  public function del($save_method) {

    $this->error = FALSE;

    // Print errors.
    if ($this->access('update', TRUE) == FALSE) {
      return FALSE;
    }

    // Delete element(s).
    if (isset($this->delta)) {
      if (!$this->cardinalityCheck(1, 'del')) {
        return FALSE;
      }
      array_splice($this->values, $this->delta, 1);
    }
    else {
      if (!$this->cardinalityCheck(0, 'set')) {
        return FALSE;
      }
      $this->values = array();
    }
    $this->count = count($this->values);

    $tmp = $this->save($save_method);
    if ($tmp === FALSE) {
      return FALSE;
    }

    $this->watchdogOp(dt('Delete'));

  }

  /**
   * Implements field element general update.
   *
   * @param string $op
   *   One of 'add', 'set'.
   * @param string $save_method
   *   Parameter to pass to $this->save().
   * @param string $values
   *   Value from commandline.
   *
   * @return bool|NULL
   *   NULL - success.
   *   FALSE - failure.
   */
  public function update($op, $save_method, $values) {

    $this->error = FALSE;

    // Print errors.
    if ($this->access('update', TRUE) == FALSE) {
      return FALSE;
    }

    $values = $this->updatePrepare($values);
    if ($values === FALSE || $this->error) {
      return FALSE;
    }
    if (empty($values)) {
      return $this->errorSet(dt('Nothing to add/set.'));
    }

    if (!$this->cardinalityCheck(count($values), $op)) {
      return FALSE;
    }

    switch ($op) {
      case 'add':
        $this->values = array_merge($this->values, $values);
        break;

      case 'set':
        $this->values = $values;
        break;

      default:
        return $this->errorSet(dt('!func: Internal error: Operation = !op.', array('!func' => __FUNCTION__, '!op' => $op)));
    }
    $this->count = count($this->values);

    if ($this->save($save_method) === FALSE) {
      return FALSE;
    }

    $this->watchdogOp($op);

  }

  /**
   * Implements field element general import.
   *
   * @param string $save_method
   *   Parameter to pass to $this->save().
   * @param array $values
   *   Value from commandline.
   *
   * @return bool|NULL
   *   NULL - success.
   *   FALSE - failure.
   */
  public function import($save_method, array $values) {

    $this->error = FALSE;

    // Print errors.
    if ($this->access('update', TRUE) == FALSE) {
      return FALSE;
    }

    $formats = array(
      'serial' => 'importSerial',
      'json' => 'importJson',
      'xml' => 'importXml',
    );
    $rx = implode('|', array_keys($formats));
    $rx = "!^(-|/.+)\.($rx)$!";
    if (!preg_match($rx, $values, $match)) {
      $e = implode(', ', array_keys($formats));
      return drush_set_error(dt('!v: Invalid value. Expected \'F.E\' where F = absolute path to existing file or - (dash) for stdin. E = extension (!e).', array('!v' => $values, '!e' => $e)));
    }
    list(, $file, $format) = $match;
    if ($file == '-') {
      $file = 'php://stdin';
    }
    else {
      $file = "$file.$format";
    }
    $data = file_get_contents($file);
    if ($data === FALSE) {
      return drush_set_error(dt('!f: Failed to read file.', array('!f' => $file)));
    }

    $method = $formats[$format];
    $values = $this->$method($data);
    if ($values === FALSE) {
      return drush_set_error(dt('Invalid input data.'));
    }
    if (!is_array($values)) {
      return drush_set_error(dt('Imported data must result an array.'));
    }
    foreach (array_keys($values) as $i) {
      if (!is_int($i)) {
        return drush_set_error(dt('Imported data array keys must be integers. Found (!t) \'!i\'.', array('!i' => $i, '!t' => gettype($i))));
      }
    }

    if (!$this->cardinalityCheck(count($values), 'set')) {
      return FALSE;
    }

    $this->values = $values;
    $this->count = count($this->values);

    if ($this->save($save_method) === FALSE) {
      return FALSE;
    }

    $this->watchdogOp('import');

  }

  /**
   * Import Serial data to array.
   *
   * @param string $data
   *   Data to be converted array.
   *
   * @return array
   *   Array - success.
   *   FALSE - failure.
   */
  private function importSerial(&$data) {
    $data = unserialize($data);
    return $data;
  }

  /**
   * Import JSON data to array.
   *
   * @param string $data
   *   Data to be converted array.
   *
   * @return array
   *   Array - success.
   *   FALSE - failure.
   */
  private function importJson(&$data) {
    $data = json_decode($data, TRUE);
    return is_null($data) ? FALSE : $data;
  }

  /**
   * Import XML data to array.
   *
   * @param string $data
   *   Data to be converted array.
   *
   * @return array
   *   Array - success.
   *   FALSE - failure.
   */
  private function importXml(&$data) {
    $xml = simplexml_load_string($data);
    if ($xml === FALSE) {
      return FALSE;
    }
    $data = array();
    drush_fields_xml_to_array($xml, $data);
    return $data;
  }

  /**
   * Logging.
   *
   * This function just simplifies logging.
   *
   * @param string $op
   *   Operation name.
   */
  protected function watchdogOp($op) {
    $link = NULL;
    $uri = entity_uri($this->entityType, $this->entity);
    if (!empty($uri['path'])) {
      $link = l(dt('View'), $uri['path']);
    }
    watchdog(
      'drush_fields',
      'Operation = !op. Entity type = !type. Bundle = !bundle. ID = !id. Field = !field.',
      array(
        '!op' => $op,
        '!type' => $this->entityType,
        '!bundle' => $this->bundle,
        '!id' => $this->entity_id,
        '!field' => $this->name,
      ),
      WATCHDOG_INFO,
      $link
    );
  }

  /**
   * Template method for field-specific implementation for update.
   *
   * This is placeholder for extended classes.
   *
   * @return array
   *   Return values to be set or added.
   */
  protected function updatePrepare() {
    return array();
  }

  /**
   * Template method for field-specific implementation for update command.
   *
   * This is placeholder for extended classes.
   *
   * @return array
   *   Return array with following keys:
   *   - description (string or array)
   *   - options (keyed array)
   *   - examples (keyed array)
   */
  public static function updateCmd() {
    return array(
      'description' => 'Example',
      'options' => array(
        'test' => 'Required. Test something. (Test)',
      ),
      'examples' => array(
        'drush test --test' => 'Do some testing.',
      ),
    );
  }

  /**
   * Implements field element 'read'.
   *
   * @param string $template
   *   Template according to which output is formatted.
   * @param bool $header
   *   TRUE - print header, FALSE - dont print header.
   *
   * @return mixed
   *   FALSE - Command failed.
   *   NULL - ok.
   */
  public function read($template, $header) {

    if (!$this->count) {
      drush_print(dt('The field is empty.'));
      return;
    }

    $template = $this->templatePrepare($template);
    if ($template === FALSE) {
      return FALSE;
    }

    // Use structural format like 'xml', 'json' etc.
    if (is_string($template)) {
      $output = $this->$template($this->values);
      // Kill extra space at end.
      $output = preg_replace('/\s+$/', '', $output);
      drush_print($output);
      return;
    }

    if (is_array($template)) {
      if ($header) {
        drush_print($this->templateApply($template, NULL));
      }
      for ($i = 0; $i < $this->count; $i++) {
        drush_print($this->templateApply($template, $i));
      }
      return;
    }

  }

  /**
   * Convert value structure into print_r format.
   *
   * @param array $values
   *   Values from $wrapper->field->value().
   *
   * @return string
   *   Formatted output.
   */
  private function readPrintR(array &$values) {
    return print_r($values, TRUE);
  }

  /**
   * Convert value structure into serial format.
   *
   * @param array $values
   *   Values from $wrapper->field->value().
   *
   * @return string
   *   Formatted output.
   */
  private function readSerial(array &$values) {
    return serialize($values);
  }

  /**
   * CONvert value structure into var_export format.
   *
   * @param array $values
   *   Values from $wrapper->field->value().
   *
   * @return string
   *   Formatted output.
   */
  private function readVarExport(array &$values) {
    return var_export($values);
  }

  /**
   * Convert value structure into var_dump format.
   *
   * @param array $values
   *   Values from $wrapper->field->value().
   *
   * @return string
   *   Formatted output.
   */
  private function readVarDump(array &$values) {
    ob_start();
    var_dump($values);
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
  }

  /**
   * Convert value structure into json format.
   *
   * @param array $values
   *   Values from $wrapper->field->value().
   *
   * @return string
   *   Formatted output.
   */
  private function readJson(array &$values) {
    return json_encode($values, JSON_PRETTY_PRINT);
  }

  /**
   * Convert value structure into XML format.
   *
   * @param array $values
   *   Values from $wrapper->field->value().
   *
   * @return string
   *   Formatted output.
   */
  private function readXml(array &$values) {

    $n = $this->name;
    $xml = new SimpleXMLElement("<$n></$n>");
    drush_fields_array_to_xml($values, $xml);
    $xml = $xml->asXML();

    // Reformat for pretty output.
    $dom = new DOMDocument('1.0');
    $dom->preserveWhiteSpace = FALSE;
    $dom->formatOutput = TRUE;
    $dom->loadXML($xml);
    $data = $dom->saveXML();

    return $data;
  }

  /**
   * Prepare template.
   *
   * @param string $template
   *   Template according to which output is formatted.
   *
   * @return array|string|bool
   *   Array - strings and formatter objects in sequence.
   *   String - Formatter method.
   *   FALSE - No formatter found.
   */
  private function templatePrepare($template) {

    if (array_key_exists($template, $this->structuralFormats)) {
      return $this->structuralFormats[$template];
    }

    return DrushFieldsReadFormatter::templatePrepare($template, $this);

  }

  /**
   * Convert prepared template plus delta-based value into output line.
   *
   * If $delta is empty - header line is assumed.
   * If $delta is supplied, get $value[$delta], format and return the line.
   *
   * @param array $template
   *   Prepared template array.
   * @param int $delta
   *   Delta which have to be processed.
   *
   * @return string
   *   Return header line or data line.
   */
  private function templateApply(array &$template, $delta) {

    $line = '';
    foreach ($template as $p) {
      switch (gettype($p)) {

        case 'string':
          $line .= $p;
          break;

        case 'object':
          if (is_null($delta)) {
            $line .= $p->head();
          }
          else {
            $line .= $p->body($delta, $this->values[$delta], $this->general);
          }
          break;

      }
    }
    return $line;

  }

  /**
   * Returns information if possible to use universal keys like ~key|key.
   *
   * @return bool
   *   TRUE - it is possible to use universal keys.
   */
  public function universalGet() {
    return TRUE;
  }

  /**
   * Returns information about structural formats like JSON or XML.
   *
   * @return array
   *   List of keywords.
   */
  public function structuralsGet() {
    return array_keys($this->structuralFormats);
  }

  /**
   * Template method for field-specific implementation for 'read' command.
   *
   * This is placeholder for extended classes.
   *
   * @return string
   *   Return template string.
   */
  public function defaultTemplate() {
    return '{%6d:delta} {%s:~}';
  }

  /**
   * Template method for field-specific implementation for 'read' command keys.
   *
   * This is placeholder for extended classes.
   *
   * @return string
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {
    return array(
      'delta' => new DrushFieldsReadFormatterDelta(),
    );
  }

}

/**
 * Convert array to SimpleXML, recursively.
 *
 * @param array $data
 *   Input.
 * @param object $xml
 *   Output, SimpleXMLElement object.
 */
function drush_fields_array_to_xml(array &$data, &$xml) {
  foreach ($data as $key => $value) {
    if (is_numeric($key)) {
      $key = "value_$key";
    }
    if (is_array($value)) {
      $subnode = $xml->addChild("$key");
      drush_fields_array_to_xml($value, $subnode);
    }
    else {
      $xml->addChild("$key", htmlspecialchars("$value"));
    }
  }
}

/**
 * Convert SimpleXML to array, recursively.
 *
 * @param object $xml
 *   Input.
 * @param array $array
 *   Output, array.
 */
function drush_fields_xml_to_array(&$xml, array &$array) {
  foreach ((array) $xml as $key => $value) {
    if (preg_match('/^value_(\d+)$/', $key, $match)) {
      $key = (int) $match[1];
    }
    if (is_object($value)) {
      $array[$key] = array();
      drush_fields_xml_to_array($value, $array[$key]);
    }
    else {
      $array[$key] = htmlspecialchars_decode($value);
    }
  }
}
