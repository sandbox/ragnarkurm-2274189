<?php

/**
 * @file
 * Node Title read functionality.
 */

require_once 'node_title.inc';

/**
 * Provide Drush commands this "plugin" can handle.
 *
 * @return array
 *   Return an array of Drush commands.
 */
function drush_fields_node_title_read_cmd() {

  return array(

    'fields-node-title-read' => array(
      'description' => 'Read a Node Title.',
      'callback' => 'drush_fields_node_title_read',
      'arguments' => array(
        'node-id' => 'Required. Example: 1234',
      ),
      'examples' => array(
        'drush fields-node-title-read 1234'
        => 'Fetch title of node 1234.',
      ),
      'required-arguments' => 1,
      /* Have to be able to use --user option. */
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
      'core' => array('7'),
      'drupal dependencies' => array(
        'entity',
        'node',
      ),
    ),

  );

}

/**
 * Perform actual title read.
 *
 * Print stdout Node title.
 *
 * @param string $nid
 *   Node ID.
 */
function drush_fields_node_title_read($nid = NULL) {

  $wrapper = drush_fields_node_title_wrapper_by_nid($nid);
  if ($wrapper === FALSE) {
    return FALSE;
  }
  if (!drush_fields_access('view', 'node', $wrapper->value(), NULL, TRUE)) {
    return FALSE;
  }
  $title = $wrapper->label();
  drush_print($title);

}
