<?php

/**
 * @file
 * Command implementations for Date* fields.
 */

class DrushFieldsFieldDate extends DrushFieldsDate {}
class DrushFieldsFieldDatestamp extends DrushFieldsDate {}
class DrushFieldsFieldDatetime extends DrushFieldsDate {}

class DrushFieldsDate extends DrushFields {

  /**
   * Field-specific command description implementation.
   *
   * @return array
   *   Field-specific command description.
   */
  static public function updateCmd() {

    return array(
      'value' => array(
        'Begin date/time. Use following formats: \'YYYY-MM-DD HH:MM:SS\', \'YYYY-MM-DDTHH:MM:SS\' or seconds since epoch. (Data, Datestamp, DateISO)',
      ),
      'options' => array(
        'value2' => 'Optional. End date/time. Use following formats: \'YYYY-MM-DD HH:MM:SS\', \'YYYY-MM-DDTHH:MM:SS\', seconds since epoch or any formats supportd by strtotime() like \'now\' or \'+1 week\'. Default = Begin = --value. (Data, Datestamp, DateISO)',
        'timezone' => 'Required. Only symbolic format. Example: Europe/Tallinn. (Data, DateStamp, DateISO)',
      ),
      'examples' => array(
        'drush fields-update node 1234 field_event --op=set --value=\'2000-01-01 10:00:00\' --value2=\'2000-01-01 11:00:00\' --timezone=\'Europe/Tallinn\''
        => 'Set field_event to \'2000-01-01 10:00:00\' - \'2000-01-01 11:00:00\' of node 1234.',
      ),
    );

  }

  /**
   * Field-specific implementation of update preparation.
   *
   * @param string $values
   *   Command-line argument --value value.
   *
   * @return array
   *   Field-specific values to set/add.
   */
  protected function updatePrepare($values) {

    $tz = drush_fields_arg_timezone();
    if ($tz === FALSE) {
      return FALSE;
    }

    $d = drush_fields_date($values, $tz);
    if ($d === FALSE) {
      return FALSE;
    }

    $value2 = drush_get_option('value2', NULL);
    if (is_string($value2)) {
      $d2 = drush_fields_date($value2, $tz);
      if ($d2 === FALSE) {
        return FALSE;
      }
    }
    else {
      $d2 = $d;
    }

    $offset = date_offset_get($d);
    $offset2 = date_offset_get($d2);

    $ref = $this->general['settings']['timezone_db'];
    $ref = new DateTimeZone($ref);

    switch ($this->fieldType) {
      case 'date':
        $format = 'Y-m-d\TH:i:s';
        break;

      case 'datetime':
        $format = 'Y-m-d H:i:s';
        break;

      case 'datestamp':
        $format = 'U';
        break;
    }

    $item = array(
      'value' => $d->setTimezone($ref)->format($format),
      'value2' => $d2->setTimezone($ref)->format($format),
      'timezone' => $tz->getName(),
      'offset' => $offset,
      'offset2' => $offset2,
    );

    return array($item);
  }

  /**
   * Field-specific 'read' template.
   *
   * @return string
   *   Return template string.
   */
  public function defaultTemplate() {
    return '{%6d:delta} {%-20s:begin-human} {%-20s:end-human} {%6s:begin-offset-human} {%6s:end-offset-human} {%s:tz}';
  }

  /**
   * Field-specific 'read' keys.
   *
   * @return array
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {

    return array(
      'delta' => new DrushFieldsReadFormatterDelta(),

      'begin-sec' => new DrushFieldsReadFormatterDateTime('U', dt('Begin'), dt('Begin, in epoch format.'), array('value')),
      'begin-human' => new DrushFieldsReadFormatterDateTime('Y-M-d H:i', dt('Begin'), dt('Begin, in human format.'), array('value')),
      'begin-offset-sec' => new DrushFieldsReadFormatterDateOffset('s', dt('B-Off'), dt('Begin offset, in seconds.'), array('offset')),
      'begin-offset-human' => new DrushFieldsReadFormatterDateOffset('hm', dt('B-Off'), dt('Begin offset, in HH:MM format.'), array('offset')),

      'end-sec' => new DrushFieldsReadFormatterDateTime('U', dt('Begin'), dt('Begin, in epoch format.'), array('value2')),
      'end-human' => new DrushFieldsReadFormatterDateTime('Y-M-d H:i', dt('Begin'), dt('Begin, in human format.'), array('value2')),
      'end-offset-sec' => new DrushFieldsReadFormatterDateOffset('s', dt('B-Off'), dt('Begin offset, in seconds.'), array('offset2')),
      'end-offset-human' => new DrushFieldsReadFormatterDateOffset('hm', dt('B-Off'), dt('Begin offset, in HH:MM format.'), array('offset2')),

      'tz' => new DrushFieldsReadFormatterValue(dt('Timezone'), dt('Timezone.'), array('timezone')),
      'rrule' => new DrushFieldsReadFormatterValue(dt('RRule'), dt('Repeat Rule.'), array('rrule')),

    );

  }

}
