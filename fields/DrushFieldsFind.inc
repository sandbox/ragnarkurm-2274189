<?php

/**
 * @file
 * Implements a class for the Template system.
 */

class DrushFieldsFind {

  private $defaultKeys = array();
  private $defaultTemplates = array(
    '{%-10s:type}',
    '{%-10s:bundle}',
    '{%6d:id}',
    '{%6d:delta}',
  );

  private $specialKeys = array();
  private $specialTemplates = array();

  /**
   * Construct DrushFieldsFind object.
   */
  public function __construct() {

    $this->defaultKeys = array(
      'bundle' => new DrushFieldsReadFormatterValue(dt('Bundle'), dt('Entity bundle.'), array('bundle')),
      'delta' => new DrushFieldsReadFormatterValue(dt('Delta'), dt('Element delta.'), array('delta')),
      'id' => new DrushFieldsReadFormatterValue(dt('ID'), dt('Entity ID.'), array('entity_id')),
      'type' => new DrushFieldsReadFormatterValue(dt('Type'), dt('Entity type.'), array('entity_type')),
      'language' => new DrushFieldsReadFormatterValue(dt('Lang'), dt('Entity language.'), array('language')),
      'revision' => new DrushFieldsReadFormatterValue(dt('Rev'), dt('Revision ID.'), array('revision_id')),
    );
  }

  /**
   * Return default template.
   *
   * @return string
   *   Template.
   */
  public function defaultTemplate() {
    $tpl = array_merge($this->defaultTemplates, $this->specialTemplates);
    $tpl = implode(' ', $tpl);
    return $tpl;
  }

  /**
   * Add field-specific template elements and keys.
   *
   * @param string $drupal_field
   *   Field name in Drupal like 'value'.
   * @param string $db_field
   *   Field name in database like 'field_text_value'.
   * @param string $type
   *   Field type: string, integer, float.
   *
   * @return bool|NULL
   *   NULL - success.
   *   FALSE - failure.
   */
  public function addSpecial($drupal_field, $db_field, $type) {
    $name = ucfirst($drupal_field);
    $desc = "$name.";
    $this->specialKeys[$drupal_field] = new DrushFieldsReadFormatterValue($name, $desc, array($db_field));
    switch ($type) {
      case 'string':
        $this->specialTemplates[] = "{%-10.10s:$drupal_field}";
        break;

      case 'integer':
        $this->specialTemplates[] = "{%6d:$drupal_field}";
        break;

      case 'float':
        $this->specialTemplates[] = "{%10.3f:$drupal_field}";
        break;

      default:
        return drush_set_error(dt('!f: Internal error.', array('!f' => __FUNCTION__)));
    }

    // PAReview.
    return NULL;
  }

  /**
   * Return information about structural formatters like JSON or XML.
   *
   * This class does not provide any.
   */
  public function structuralsGet() {
  }

  /**
   * Construct formatter keys.
   *
   * @return array
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {
    return array_merge($this->defaultKeys, $this->specialKeys);
  }

  /**
   * Returns information if possible to use universal keys like ~key|key.
   *
   * @return bool
   *   FALSE - it is not possible to use universal keys.
   */
  public function universalGet() {
    return FALSE;
  }

}
