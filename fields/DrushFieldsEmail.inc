<?php

/**
 * @file
 * Command implementations for Email field.
 */

class DrushFieldsFieldEmail extends DrushFields {

  /**
   * Field-specific command description implementation.
   *
   * @return array
   *   Field-specific command description.
   */
  static public function updateCmd() {

    return array(
      'value' => array(
        'Multiple email values are separated by comma. (Email)',
      ),
      'examples' => array(
        'drush fields-update node 1234 field_email --op=set --value=\'john.doe@example.com\''
        => 'Set field_email to \'john.doe@example.com\' of node 1234.',
      ),
    );

  }

  /**
   * Field-specific implementation of update preparation.
   *
   * @param string $values
   *   Command-line argument --value value.
   *
   * @return array
   *   Field-specific values to set/add.
   */
  protected function updatePrepare($values) {

    $values = preg_replace('/\s+/', '', $values);
    $values = explode(',', $values);

    return $values;

  }

  /**
   * Field-specific 'read' template.
   *
   * @return string
   *   Return template string.
   */
  public function defaultTemplate() {
    return '{%6d:delta} {%s:email}';
  }

  /**
   * Field-specific 'read' keys.
   *
   * @return array
   *   Return associative array.
   *   Keys are template keys.
   *   Values are Formatters objects.
   */
  public function keysGet() {
    return array(
      'delta' => new DrushFieldsReadFormatterDelta(),
      'email' => new DrushFieldsReadFormatterValue(dt('E-mail'), dt('E-mail address.')),
    );
  }

}
