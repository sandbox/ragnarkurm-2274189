<?php

/**
 * @file
 * Field (element) count functionality.
 */

/**
 * Provide Drush commands this "plugin" can handle.
 *
 * @return array
 *   Return an array of Drush commands.
 */
function drush_fields_count_cmd() {

  return array(

    'fields-count' => array(
      'description' => 'Count element(s) in a field.',
      'callback' => 'drush_fields_count',
      'arguments' => array(
        'entity type machine name' => 'Required. Example: node',
        'entity id' => 'Required. Example: 1234',
        'field machine name' => 'Required. Example: field_measure',
      ),
      'examples' => array(
        'drush fields-count node 1234 field_measure'
        => 'Count number of elements in field_measure.',
      ),
      'required-arguments' => 3,
      /* Have to be able to use --user option. */
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
      'core' => array('7'),
      'drupal dependencies' => array(
        'entity',
        // And a field module.
      ),
    ),

  );

}

/**
 * Implements field element count.
 *
 * @param string $entity_type
 *   Entity machine name. Examples: node, profile2.
 * @param string|int $entity_id
 *   Entity ID (nid, uid, etc). Examples: 1, 10, 100, 31415.
 * @param string $field_name
 *   Field machine name. Example: field_measure.
 */
function drush_fields_count($entity_type = NULL, $entity_id = NULL, $field_name = NULL) {

  // Use general field since count applies to all fields.
  $field = new DrushFields($entity_type, $entity_id, $field_name);
  if ($field->errorGet() === TRUE) {
    return FALSE;
  }

  $field->count();

}
