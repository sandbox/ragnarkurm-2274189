<?php

/**
 * @file
 * Node Title set functionality.
 */

require_once 'node_title.inc';

/**
 * Provide Drush commands this "plugin" can handle.
 *
 * @return array
 *   Return an array of Drush commands.
 */
function drush_fields_node_title_set_cmd() {

  return array(

    'fields-node-title-set' => array(
      'description' => 'Set a Node Title.',
      'callback' => 'drush_fields_node_title_set',
      'arguments' => array(
        'node-id' => 'Required. Example: 1234',
      ),
      'options' => array(
        'value' => 'The title of node.',
      ),
      'examples' => array(
        'drush fields-node-title-set 1234 --value="Automated title"'
        => 'Set title of node 1234 to "Automate title".',
      ),
      'required-arguments' => 1,
      /* Have to be able to use --user option. */
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
      'core' => array('7'),
      'drupal dependencies' => array(
        'entity',
        'node',
      ),
    ),

  );

}

/**
 * Perform actual title set.
 *
 * Print stdout Node title.
 *
 * @param string $nid
 *   Node ID.
 */
function drush_fields_node_title_set($nid = NULL) {

  $value = drush_get_option('value', '');
  // Strip extra space.
  $value = preg_replace('/\s+/', ' ', $value);
  $value = preg_replace('/^\s+|\s+$/', '', $value);
  if (empty($value)) {
    return drush_set_error(dt('--value: Required.'));
  }

  $wrapper = drush_fields_node_title_wrapper_by_nid($nid);
  if ($wrapper === FALSE) {
    return FALSE;
  }
  if (!drush_fields_access('update', 'node', $wrapper->value(), NULL, TRUE)) {
    return FALSE;
  }
  $wrapper->title->set($value);

  if (!drush_confirm(dt('Set node title?'))) {
    drush_user_abort();
  }

  $wrapper->save();

  $link = NULL;
  $uri = entity_uri('node', $wrapper->value());
  if (!empty($uri['path'])) {
    $link = l(dt('View'), $uri['path']);
  }
  watchdog(
    'drush_fields',
    'Operation = !op. Entity type = !type. Bundle = !bundle. ID = !id. Field = !field.',
    array(
      '!op' => dt('Update'),
      '!type' => $wrapper->type(),
      '!bundle' => $wrapper->getBundle(),
      '!id' => $wrapper->getIdentifier(),
      '!field' => 'title',
    ),
    WATCHDOG_INFO,
    $link
  );

}
