<?php

/**
 * @file
 * Field (element) deletion functionality.
 */

/**
 * Provide Drush commands this "plugin" can handle.
 *
 * @return array
 *   Return an array of Drush commands.
 */
function drush_fields_del_cmd() {

  return array(

    'fields-del' => array(
      'description' => 'Delete element(s) from a field.',
      'callback' => 'drush_fields_del',
      'arguments' => array(
        'entity type machine name' => 'Required. Example: node',
        'entity id' => 'Required. Example: 1234',
        'field machine name' => 'Required. Example: field_measure',
        'delta' => 'Optional. Default = All. Example: 3',
      ),
      'options' => array(
        'save' => drush_fields_save_options_description(),
      ),
      'examples' => array(
        'drush fields-del node 1234 field_measure 3'
        => 'Delete a fourth (delta=index=3) element from field_measure.',
      ),
      'required-arguments' => 3,
      /* Have to be able to use --user option. */
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
      'core' => array('7'),
      'drupal dependencies' => array(
        'entity',
        // And a field module.
      ),
    ),

  );

}

/**
 * Implements field element deletion.
 *
 * @param string $entity_type
 *   Entity machine name. Examples: node, profile2.
 * @param string|int $entity_id
 *   Entity ID (nid, uid, etc). Examples: 1, 10, 100, 31415.
 * @param string $field_name
 *   Field machine name. Example: field_measure.
 * @param string|int $delta
 *   Delta of the multivalue field element, may be NULL.
 */
function drush_fields_del($entity_type = NULL, $entity_id = NULL, $field_name = NULL, $delta = NULL) {

  // Use general field since count applies to all fields.
  $field = new DrushFields($entity_type, $entity_id, $field_name, $delta);
  if ($field->errorGet() === TRUE) {
    return FALSE;
  }

  $save_method = drush_fields_arg_save();
  if ($save_method === FALSE) {
    return FALSE;
  }

  $field->del($save_method);

}
