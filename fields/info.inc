<?php

/**
 * @file
 * Field info: general or instance.
 */

/**
 * Provide Drush commands this "plugin" can handle.
 *
 * @return array
 *   Return an array of Drush commands.
 */
function drush_fields_info_cmd() {

  return array(

    'fields-info' => array(
      'description' => 'Inspect field info: general (field_info_field()) or instance (field_info_instance()).',
      'callback' => 'drush_fields_info',
      'arguments' => array(
        'field machine name' => 'Required. Example: field_measure',
        'entity type machine name' => 'Optional. Example: node',
        'bundle machine name' => 'Optional. Example: page',
      ),
      'examples' => array(
        'drush fields-info field_measure'
        => 'Inspect general field info.',
        'drush fields-info field_measure node page'
        => 'Inspect instance field info.',
      ),
      'required-arguments' => 1,
      /* Have to be able to use --user option. */
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
      'core' => array('7'),
      'drupal dependencies' => array(
        'entity',
        // And a field module.
      ),
    ),

  );

}

/**
 * Implements field info.
 *
 * @return bool|NULL
 *   NULL - success.
 *   FALSE - failure.
 */
function drush_fields_info() {

  $args = drush_get_arguments();
  // Drop command.
  array_shift($args);
  $c = count($args);

  if ($c == 1) {
    list($field_name) = $args;
    return drush_fields_info_general($field_name);
  }

  if ($c == 3) {
    list($field_name, $entity_type, $bundle) = $args;
    return drush_fields_info_instance($field_name, $entity_type, $bundle);
  }

  return drush_set_error(dt('Expected 1 (for general info) or 3 (for field instance) arguments.'));

}

/**
 * Implement field instance info.
 *
 * @param string $field_name
 *   Field name.
 * @param string $entity_type
 *   Entity type.
 * @param string $bundle
 *   Bundle.
 *
 * @return bool|NULL
 *   NULL - success.
 *   FALSE - failure.
 */
function drush_fields_info_instance($field_name, $entity_type, $bundle) {

  $field = field_info_field($field_name);
  if (empty($field)) {
    drush_set_error(dt('!f: No such field.', array('!f' => $field_name)));
    $fields = field_info_fields();
    return drush_fields_options(array_keys($fields));
  }

  $entity = entity_get_info($entity_type);
  if (empty($entity)) {
    drush_set_error(dt('!e: No such entity type.', array('!e' => $entity_type)));
    $entities = entity_get_info();
    return drush_fields_options(array_keys($entities));
  }

  if (!array_key_exists($bundle, $entity['bundles'])) {
    drush_set_error(dt('!b: No such bundle in entity type \'!e\'.', array('!b' => $bundle, '!e' => $entity_type)));
    return drush_fields_options(array_keys($entity['bundles']));
  }

  if (!in_array($bundle, $field['bundles'][$entity_type])) {
    return drush_set_error(dt('Field \'!f\' does not belong to \'!e\'/\'!b\'.', array(
      '!b' => $bundle,
      '!e' => $entity_type,
      '!f' => $field_name,
    )));
  }

  $instance = field_info_instance($entity_type, $field_name, $bundle);
  $instance = print_r($instance, TRUE);
  $instance = preg_replace('/\s+$/', '', $instance);
  drush_print($instance);
}

/**
 * Implement field general info.
 *
 * @param string $field_name
 *   Field name.
 *
 * @return bool|NULL
 *   NULL - success.
 *   FALSE - failure.
 */
function drush_fields_info_general($field_name) {

  $field = field_info_field($field_name);

  if (empty($field)) {
    drush_set_error(dt('!f: No such field.', array('!f' => $field_name)));
    $fields = field_info_fields();
    return drush_fields_options(array_keys($fields));
  }

  $field = print_r($field, TRUE);
  $field = preg_replace('/\s+$/', '', $field);
  drush_print($field);
}
